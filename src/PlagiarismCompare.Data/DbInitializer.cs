using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Data;

public class DbInitializer
{
    public static void Initialize(DataContext context)
    {
        // Return if data added before
        if (context.Students.Any() || context.Modules.Any())
        {
            return;
        }
        
        // Add students
        var john = new Student() {FirstName = "John", LastName = "Smith", Code = "sc01js", StudentId = 123456789};
        context.Students.Add(john);
        var jack = new Student() {FirstName="Jack", LastName="Frost", Code="sc02jf", StudentId=234567890};
        context.Students.Add(jack);
        context.SaveChanges();
        
        // Add modules
        var os = new Module() {Name = "Operating Systems", Code = "COMP2211"};
        context.Modules.Add(os);
        var compilers = new Module() {Name = "Compiler Design and Construction", Code = "COMP2932"};
        context.Modules.Add(compilers);
        context.SaveChanges();
        
        // Add module semesters
        var os2010 = new Semester() {Year = 2010, Term = Term.First};
        os.Semesters.Add(os2010);
        var os2011 = new Semester() {Year = 2011, Term = Term.First};
        os.Semesters.Add(os2011);
    
        var compilers2010 = new Semester() {Year = 2010, Term = Term.Second};
        compilers.Semesters.Add(compilers2010);
        var compilers2011 = new Semester() {Year = 2011, Term = Term.Second};
        compilers.Semesters.Add(compilers2011);
        context.SaveChanges();
        
        // Add students to some module semesters
        jack.SemesterStudents.Add(new SemesterStudent(){Semester = os2010});
        jack.SemesterStudents.Add(new SemesterStudent(){Semester = compilers2010});
        
        john.SemesterStudents.Add(new SemesterStudent(){Semester = os2011});
        john.SemesterStudents.Add(new SemesterStudent(){Semester = compilers2011});
    
        context.SaveChanges();
    }
}
