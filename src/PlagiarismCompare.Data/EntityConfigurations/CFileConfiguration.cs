using CTools;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Data.EntityConfigurations;

public class CFileConfiguration : IEntityTypeConfiguration<CFile>
{
    public void Configure(EntityTypeBuilder<CFile> builder)
    {
        JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            NullValueHandling = NullValueHandling.Ignore,
            FloatFormatHandling = FloatFormatHandling.String
        };
        
        
        ValueConverter<IList<Token>, String> converter = new ValueConverter<IList<Token>, string>
        (
            v => JsonConvert.SerializeObject(v, settings), 
            v => JsonConvert.DeserializeObject<IList<Token>>(v, settings) ?? new List<Token>()
        );
        
        builder.Property(cFile => cFile.Tokens).HasConversion(converter);
        builder.Property(cFile => cFile.Tokens).Metadata.SetValueConverter(converter);

        ValueComparer<IList<Token>> comparer = new ValueComparer<IList<Token>>
        (
            (l, r) => JsonConvert.SerializeObject(l) == JsonConvert.SerializeObject(r),
            v => JsonConvert.SerializeObject(v).GetHashCode(),
            v => JsonConvert.DeserializeObject<IList<Token>>(JsonConvert.SerializeObject(v))!
        );
        builder.Property(cFile => cFile.Tokens).Metadata.SetValueComparer(comparer);
    }
}