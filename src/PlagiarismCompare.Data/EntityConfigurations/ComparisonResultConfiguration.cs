using CTools;
using CTools.Comparison;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Newtonsoft.Json;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Data.EntityConfigurations;

public class ComparisonResultConfiguration : IEntityTypeConfiguration<ComparisonResult>
{
    public void Configure(EntityTypeBuilder<ComparisonResult> builder)
    {
        JsonSerializerSettings settings = new JsonSerializerSettings() {NullValueHandling = NullValueHandling.Ignore};
        ValueConverter<IList<TokenMatch>, String> converter = new ValueConverter<IList<TokenMatch>, string>
        (
            v => JsonConvert.SerializeObject(v, settings), 
            v => JsonConvert.DeserializeObject<IList<TokenMatch>>(v, settings) ?? new List<TokenMatch>()
        );
        
        builder.Property(res => res.Matches).HasConversion(converter);
        builder.Property(res => res.Matches).Metadata.SetValueConverter(converter);

        ValueComparer<IList<TokenMatch>> comparer = new ValueComparer<IList<TokenMatch>>
        (
            (l, r) => JsonConvert.SerializeObject(l) == JsonConvert.SerializeObject(r),
            v => JsonConvert.SerializeObject(v).GetHashCode(),
            v => JsonConvert.DeserializeObject<IList<TokenMatch>>(JsonConvert.SerializeObject(v))!
        );
        builder.Property(res => res.Matches).Metadata.SetValueComparer(comparer);
    }
}