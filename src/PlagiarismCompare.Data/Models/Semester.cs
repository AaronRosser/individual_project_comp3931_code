using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PlagiarismCompare.Data.Models;

public enum Term
{
    First, Second
}

public class Semester : BaseEntity
{
    public Semester()
    {
        SemesterStudents = new HashSet<SemesterStudent>();
    }
    
    public int Year { get; set; }
    [JsonConverter(typeof(StringEnumConverter))]
    public Term Term { get; set; }
    
    public Module Module { get; set; } = null!;
    [JsonIgnore]
    public ICollection<SemesterStudent> SemesterStudents { get; set; }

    public override string ToString()
    {
        return $"{Module.Code} - {Module.Name} - {Year}({Term})";
    }
}