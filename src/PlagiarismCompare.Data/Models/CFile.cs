using CTools;
using CTools.Comparison;
using Newtonsoft.Json;

namespace PlagiarismCompare.Data.Models;

public class CFile : BaseEntity
{
    public CFile()
    {
        ComparisonResults = new HashSet<ComparisonResult>();
    }
    
    public string FilePath { get; set; } = null!;

    public Submission Submission { get; set; } = null!;
    public int NumLines { get; set; }
    public int NumCharacters { get; set; }
    public int NumTokens { get; set; }
    public string? Error { get; set; }
    public IList<Token> Tokens { get; set; } = null!;
    [JsonIgnore]
    public ICollection<ComparisonResult> ComparisonResults { get; set; }

    public void AddComparison(CFile otherFile)
    {
        // Check if already compared
        if (ComparisonResults.Any(res => res.CFiles.Any(oth => oth.Id == otherFile.Id)))
            return;

        // Compare files
        var matches = TokenComparer.CompareFileTokens(this.Tokens, otherFile.Tokens)
            .OrderByDescending(m => m.NumberMatchingTokens).ToList();
        
        // Dont add empty matches to database
        if (matches.Count == 0)
            return;
        
        var longestMatch = matches.Any() ? matches.Max(m => m.NumberMatchingTokens) : 0;
        // var totalMatchingTokens = matches.Sum(m => m.NumberMatchingTokens);
        float similarity = (float) longestMatch / Math.Max(Tokens.Count, otherFile.Tokens.Count);

        // Add to comparison results
        ComparisonResult result = new ComparisonResult()
        {
            LongestTokenMatch = longestMatch,
            Similarity = similarity,
            Matches = matches,
            MatchesMain = Id,
            CFiles = new List<CFile>()
            {
                this, 
                otherFile
            }
        };
        ComparisonResults.Add(result);
    }
}