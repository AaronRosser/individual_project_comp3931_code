using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace PlagiarismCompare.Data.Models;

public class Student : BaseEntity
{
    public Student()
    {
        SemesterStudents = new HashSet<SemesterStudent>();
    }
    [Display(Name = "First Name")]  
    public string FirstName { get; set; } = null!;
    [Display(Name = "Last Name")]  
    public string LastName { get; set; } = null!;

    public string Name => $"{FirstName} {LastName}";

    public string Code { get; set; } = null!;
    [Display(Name = "Student ID")]  
    public int StudentId { get; set; }
    [JsonIgnore]
    public ICollection<SemesterStudent> SemesterStudents { get; set; }

    public override string ToString()
    {
        return $"{Name} - {Code} - {StudentId}";
    }
}