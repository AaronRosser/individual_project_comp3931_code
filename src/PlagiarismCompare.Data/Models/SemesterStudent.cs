using Newtonsoft.Json;

namespace PlagiarismCompare.Data.Models;

public class SemesterStudent : BaseEntity
{
    public SemesterStudent()
    {
        Submissions = new HashSet<Submission>();
    }
    
    public Student Student { get; set; } = null!;
    public Semester Semester { get; set; } = null!;
    [JsonIgnore]
    public ICollection<Submission> Submissions { get; set; }
}