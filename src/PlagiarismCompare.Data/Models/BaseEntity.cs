using System.ComponentModel.DataAnnotations;

namespace PlagiarismCompare.Data.Models;

public abstract class BaseEntity
{
    [Key]
    public int Id { get; set; }
}