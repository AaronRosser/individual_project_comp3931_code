using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PlagiarismCompare.Data.Models;

public enum SubmissionState
{
    Submitted,
    Parsed,
    Processed
}

public class Submission : BaseEntity
{
    public Submission()
    {
        CFiles = new HashSet<CFile>();
    }
    
    public string Name { get; set; } = null!;
    public string FileNamePrefix { get; set; } = null!;
    [JsonConverter(typeof(StringEnumConverter))]
    public SubmissionState State { get; set; }
    public DateTime Submitted { get; set; }
    
    // Parsing timing
    public DateTime? ParsingStart { get; set; }
    
    // Comparison timing
    public DateTime? ComparisonStart { get; set; }
    public DateTime? ComparisonEnd { get; set; }
    public double? ComparisonSeconds { get; set; }
    
    public SemesterStudent SemesterStudent { get; set; } = null!;
    [JsonIgnore]
    public ICollection<CFile> CFiles { get; set; }

    public void CompareSubmissions(Submission otherSubmission)
    {
        // Check if already compared submission

        // Loop over all files
        foreach (var cFile in CFiles)
        {
            foreach (var otherCFile in otherSubmission.CFiles)
            {
                cFile.AddComparison(otherCFile);
            }
        }
    }
}