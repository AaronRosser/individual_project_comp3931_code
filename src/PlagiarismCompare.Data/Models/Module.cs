using Newtonsoft.Json;

namespace PlagiarismCompare.Data.Models;

public class Module : BaseEntity
{
    public Module()
    {
        Semesters = new HashSet<Semester>();
    }
    
    public string Code { get; set; } = null!;
    public string Name { get; set; } = null!;
    [JsonIgnore]
    public ICollection<Semester> Semesters { get; set; }
}