using CTools.Comparison;

namespace PlagiarismCompare.Data.Models;

public class ComparisonResult : BaseEntity
{
    public ComparisonResult()
    {
        CFiles = new HashSet<CFile>();
    }
    
    public float Similarity { get; set; }
    public int LongestTokenMatch { get; set; }
    public IList<TokenMatch> Matches { get; set; } = null!;
    public int MatchesMain { get; set; }
    
    public ICollection<CFile> CFiles { get; set; }

    public (CFile, CFile) GetCFiles()
    {
        return (CFiles.First(), CFiles.Skip(1).First());
    }

    public CFile GetOtherCFile(CFile file)
    {
        return CFiles.First(c => c.Id != file.Id);
    }
}