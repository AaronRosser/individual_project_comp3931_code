using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Data;

public class DataContext : IdentityDbContext<User>
{
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }

    public DbSet<Student> Students { get; set; } = null!;
    
    public DbSet<Module> Modules { get; set; } = null!;
    public DbSet<Semester> ModuleSemesters { get; set; } = null!;
    
    public DbSet<SemesterStudent> SemesterStudents { get; set; } = null!;
    
    public DbSet<Submission> Submissions { get; set; } = null!;
    public DbSet<CFile> CFiles { get; set; } = null!;
    public DbSet<ComparisonResult> ComparisonResults { get; set; } = null!;
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataContext).Assembly);
    }
}