﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PlagiarismCompare.Data.Migrations
{
    public partial class Added_lexer_exception_string : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Error",
                table: "CFiles",
                type: "TEXT",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Error",
                table: "CFiles");
        }
    }
}
