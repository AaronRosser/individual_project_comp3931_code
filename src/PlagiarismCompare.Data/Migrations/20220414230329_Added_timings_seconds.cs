﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PlagiarismCompare.Data.Migrations
{
    public partial class Added_timings_seconds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "ComparisonSeconds",
                table: "Submissions",
                type: "REAL",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ComparisonSeconds",
                table: "Submissions");
        }
    }
}
