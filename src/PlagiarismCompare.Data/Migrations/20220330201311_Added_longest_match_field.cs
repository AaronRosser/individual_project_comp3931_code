﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PlagiarismCompare.Data.Migrations
{
    public partial class Added_longest_match_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "LongestTokenMatch",
                table: "ComparisonResults",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Matches",
                table: "ComparisonResults",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LongestTokenMatch",
                table: "ComparisonResults");

            migrationBuilder.DropColumn(
                name: "Matches",
                table: "ComparisonResults");
        }
    }
}
