﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace PlagiarismCompare.Data.Migrations
{
    public partial class Added_order_to_matches : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MatchesMain",
                table: "ComparisonResults",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MatchesMain",
                table: "ComparisonResults");
        }
    }
}
