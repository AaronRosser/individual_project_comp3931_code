namespace CTools;

public enum TokenType
{
    Identifier,
    Keyword,
    IntegerConstant,
    FloatConstant,
    CharacterConstant,
    String,
    Punctuator,
    PreprocessorKeyword,
    HeaderName,
    PpNumber
}