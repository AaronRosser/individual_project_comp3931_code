namespace CTools.Lexer;

public class CFileInfo
{
    public int NumLines { get; set; }
    public int NumCharacters { get; set; }
}