using CTools.Lexer;

namespace CTools;

public record Token
{
    public bool preprocessor { get; init; }
    public TokenType type { get; init; }
    public string text { get; init; } = null!;
    // Store enum for punctuators and keywords
    public ConstTokenValue? constantTokenValue { get; init; }
    // Store value of constant integers, floats and characters
    public double? constantValue { get; init; }

    public int lineNumber { get; init; }
    public int characterNumber { get; init; }
    public bool skippable { get; set; }

    public override string ToString()
    {
        return $"<Token, {type}, \"{text}\", {lineNumber}>";
    }
}