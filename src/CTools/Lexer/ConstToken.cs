namespace CTools.Lexer;

public record ConstToken(string Text, ConstTokenValue Value, bool Skippable = false);