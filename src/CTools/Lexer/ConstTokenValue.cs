namespace CTools.Lexer;

public enum ConstTokenValue
{
    //! Operators
    // Arithmetic
    Add,
    Sub,
    Star, // Multiply and pointer dereference
    Divide,
    Mod,
    Increment,
    Decrement,
    
    // Relational
    EqualTo,
    NotEqualTo,
    LessThan,
    GreaterThan,
    LessThanEqualTo,
    GreaterThanEqualTo,
    
    // Logical
    LogicalAnd,
    LogicalOr,
    LogicalNot,
    
    // Bitwise
    Ampersand, // And and get address
    Or,
    Xor,
    Not,
    LShit,
    RShift,
    
    // Assignment
    AddAssign,
    SubAssign,
    MultiplyAssign,
    DivideAssign,
    ModAssign,
    
    LShiftAssign,
    RShiftAssign,
    
    AndAssign,
    XorAssign,
    OrAssign,

    // Preprocessor
    Hash,
    DoubleHash,
    
    // Punctuators
    OpenBracket,
    CloseBracket,
    OpenBrace,
    CloseBrace,
    OpenParenthesis,
    CloseParenthesis,
    
    Semicolon,
    Comma,
    
    // Misc
    Assign,
    Dot,
    Arrow,
    QuestionMark,
    Colon,
    Ellipses,
    
    //! Keywords
    StaticAssert,
    ThreadLocal,
    Imaginary,
    Noreturn,
    Continue,
    Register,
    Restrict,
    Unsigned,
    Volatile,
    Alignas,
    Alignof,
    Complex,
    Generic,
    Default,
    Typedef,
    Atomic,
    Double,
    Extern,
    Inline,
    Return,
    Signed,
    Sizeof,
    Static,
    Struct,
    Switch,
    Break,
    Const,
    Float,
    Short,
    Union,
    While,
    Bool,
    Auto,
    Case,
    Char,
    Else,   // C and preprocessor
    Enum,
    Goto,
    Long,
    Void,
    For,
    Int,
    Do,
    If,     // C and preprocessor
    
    //! Preprocessor keywords
    Include,
    Ifndef,
    Define,
    Pragma,
    Ifdef,
    Endif,
    Undef,
    Error,
    Elif,
    Line,
    
    //! Identifier
    Identifier
}