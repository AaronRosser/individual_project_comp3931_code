using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;

namespace CTools.Lexer;

public class Lexer
{
    private delegate Token? GetTokenMethod();

    private static readonly ConstToken[] Punctuators = new[]
    {
        new ConstToken("%:%:", ConstTokenValue.DoubleHash),
        new ConstToken("...", ConstTokenValue.Ellipses),
        
        new ConstToken("<<=", ConstTokenValue.LShiftAssign),
        new ConstToken(">>=", ConstTokenValue.RShiftAssign),
        
        new ConstToken("->", ConstTokenValue.Arrow),
        
        new ConstToken("++", ConstTokenValue.Increment),
        new ConstToken("--", ConstTokenValue.Decrement),
        
        new ConstToken("+=", ConstTokenValue.AddAssign),
        new ConstToken("-=", ConstTokenValue.SubAssign),
        new ConstToken("*=", ConstTokenValue.MultiplyAssign),
        new ConstToken("/=", ConstTokenValue.DivideAssign),
        new ConstToken("%=", ConstTokenValue.ModAssign),
        
        new ConstToken("&=", ConstTokenValue.AndAssign),
        new ConstToken("|=", ConstTokenValue.OrAssign),
        new ConstToken("^=", ConstTokenValue.XorAssign),

        new ConstToken("<<", ConstTokenValue.LShit),
        new ConstToken(">>", ConstTokenValue.RShift),
        
        new ConstToken("==", ConstTokenValue.EqualTo),
        new ConstToken("!=", ConstTokenValue.NotEqualTo),
        new ConstToken("<=", ConstTokenValue.LessThanEqualTo),
        new ConstToken(">=", ConstTokenValue.GreaterThanEqualTo),
        
        new ConstToken("&&", ConstTokenValue.LogicalAnd),
        new ConstToken("||", ConstTokenValue.LogicalOr),
        
        new ConstToken("##", ConstTokenValue.DoubleHash),
        
        new ConstToken("<:", ConstTokenValue.OpenBracket),
        new ConstToken(":>", ConstTokenValue.CloseBracket),
        new ConstToken("<%", ConstTokenValue.OpenBrace, Skippable:true),
        new ConstToken("%>", ConstTokenValue.CloseBrace, Skippable:true),
        new ConstToken("%:", ConstTokenValue.Hash),
        
        new ConstToken("[", ConstTokenValue.OpenBracket),
        new ConstToken("]", ConstTokenValue.CloseBracket),
        new ConstToken("{", ConstTokenValue.OpenBrace, Skippable:true),
        new ConstToken("}", ConstTokenValue.CloseBrace, Skippable:true),
        new ConstToken("(", ConstTokenValue.OpenParenthesis),
        new ConstToken(")", ConstTokenValue.CloseParenthesis),
        
        new ConstToken("&", ConstTokenValue.Ampersand),
        new ConstToken("|", ConstTokenValue.Or),
        new ConstToken("^", ConstTokenValue.Xor),
        new ConstToken("~", ConstTokenValue.Not),
        
        new ConstToken("+", ConstTokenValue.Add),
        new ConstToken("-", ConstTokenValue.Sub),
        new ConstToken("*", ConstTokenValue.Star),
        new ConstToken("/", ConstTokenValue.Divide),
        new ConstToken("%", ConstTokenValue.Mod),
        
        new ConstToken("!", ConstTokenValue.LogicalNot),
        new ConstToken("<", ConstTokenValue.LessThan),
        new ConstToken(">", ConstTokenValue.GreaterThan),
        
        new ConstToken("?", ConstTokenValue.QuestionMark),
        new ConstToken(":", ConstTokenValue.Colon),
        
        new ConstToken("=", ConstTokenValue.Assign),
        new ConstToken(".", ConstTokenValue.Dot),
        
        new ConstToken(";", ConstTokenValue.Semicolon),
        new ConstToken(",", ConstTokenValue.Comma),
        new ConstToken("#", ConstTokenValue.Hash)
    };

    private static readonly ConstToken[] Keywords = new[]
    {
        new ConstToken("_Static_assert", ConstTokenValue.StaticAssert, Skippable:true),
        new ConstToken("_Thread_local", ConstTokenValue.ThreadLocal, Skippable:true),
        new ConstToken("_Imaginary", ConstTokenValue.Imaginary, Skippable:true),
        new ConstToken("_Noreturn", ConstTokenValue.Noreturn, Skippable:true),
        new ConstToken("continue", ConstTokenValue.Continue),
        new ConstToken("register", ConstTokenValue.Register, Skippable:true),
        new ConstToken("restrict", ConstTokenValue.Restrict, Skippable:true),
        new ConstToken("unsigned", ConstTokenValue.Unsigned),
        new ConstToken("volatile", ConstTokenValue.Volatile, Skippable:true),
        new ConstToken("_Alignas", ConstTokenValue.Alignas, Skippable:true),
        new ConstToken("_Alignof", ConstTokenValue.Alignof, Skippable:true),
        new ConstToken("_Complex", ConstTokenValue.Complex, Skippable:true),
        new ConstToken("_Generic", ConstTokenValue.Generic, Skippable:true),
        new ConstToken("default", ConstTokenValue.Default),
        new ConstToken("typedef", ConstTokenValue.Typedef),
        new ConstToken("_Atomic", ConstTokenValue.Atomic, Skippable:true),
        new ConstToken("double", ConstTokenValue.Double),
        new ConstToken("extern", ConstTokenValue.Extern),
        new ConstToken("inline", ConstTokenValue.Inline, Skippable:true),
        new ConstToken("return", ConstTokenValue.Return),
        new ConstToken("signed", ConstTokenValue.Signed, Skippable:true),
        new ConstToken("sizeof", ConstTokenValue.Sizeof),
        new ConstToken("static", ConstTokenValue.Static, Skippable:true),
        new ConstToken("struct", ConstTokenValue.Struct),
        new ConstToken("switch", ConstTokenValue.Switch),
        new ConstToken("break", ConstTokenValue.Break),
        new ConstToken("const", ConstTokenValue.Const, Skippable:true),
        new ConstToken("float", ConstTokenValue.Float),
        new ConstToken("short", ConstTokenValue.Short),
        new ConstToken("union", ConstTokenValue.Union),
        new ConstToken("while", ConstTokenValue.While),
        new ConstToken("_Bool", ConstTokenValue.Bool, Skippable:true),
        new ConstToken("auto", ConstTokenValue.Auto),
        new ConstToken("case", ConstTokenValue.Case),
        new ConstToken("char", ConstTokenValue.Char),
        new ConstToken("else", ConstTokenValue.Else),
        new ConstToken("enum", ConstTokenValue.Enum),
        new ConstToken("goto", ConstTokenValue.Goto),
        new ConstToken("long", ConstTokenValue.Long),
        new ConstToken("void", ConstTokenValue.Void),
        new ConstToken("for", ConstTokenValue.For),
        new ConstToken("int", ConstTokenValue.Int),
        new ConstToken("do", ConstTokenValue.Do),
        new ConstToken("if", ConstTokenValue.If)
    };

    private static readonly ConstToken[] PreprocessorKeywords = new[]
    {
        new ConstToken("include", ConstTokenValue.Include),
        new ConstToken("ifndef", ConstTokenValue.Ifndef),
        new ConstToken("define", ConstTokenValue.Define),
        new ConstToken("pragma", ConstTokenValue.Pragma),
        new ConstToken("ifdef", ConstTokenValue.Ifdef),
        new ConstToken("endif", ConstTokenValue.Endif),
        new ConstToken("undef", ConstTokenValue.Undef),
        new ConstToken("error", ConstTokenValue.Error),
        new ConstToken("elif", ConstTokenValue.Elif),
        new ConstToken("else", ConstTokenValue.Else),
        new ConstToken("line", ConstTokenValue.Line),
        new ConstToken("if", ConstTokenValue.If)
    };

    private int _position;
    private int _lineNumber;
    private int _consecutiveEscapedNewLines;
    private bool _preprocessorLine;

    private readonly int _totalNumberLines;
    
    private readonly string _fileText;
    private readonly string _filePath;

    private Token? _previousToken;
    private readonly List<Token> _tokens;
    
    public Lexer(string filePath) : this(filePath, File.ReadAllText(filePath).ReplaceLineEndings())
    {
        
    }

    public Lexer(string filePath, string fileText)
    {
        _filePath = filePath;
        _fileText = fileText;

        _position = 0;
        _lineNumber = 1;
        _consecutiveEscapedNewLines = 0;

        _preprocessorLine = false;
        _previousToken = null;
        
        foreach (char c in fileText) 
            if (c == '\n') _totalNumberLines++;
        
        _tokens = new List<Token>();
    }

    private bool IsEof() => _position >= _fileText.Length;
    private static bool IsOctalDigit(char c) => c is >= '0' and <= '7';
    private static bool IsNonZeroDigit(char c) => c is >= '1' and <= '9';
    private static bool IsDigit(char c) => IsNonZeroDigit(c) || c == '0';
    private static bool IsHexadecimalDigit(char c) => IsDigit(c) || 
                                                      c is >= 'A' and <= 'F' 
                                                          or >= 'a' and <= 'f';

    private static bool IsNonDigit(char c) => c is >= 'A' and <= 'Z' or >= 'a' and <= 'z' or '_';
    
    // From https://stackoverflow.com/questions/1214980/convert-a-single-hex-character-to-its-byte-value-in-c-sharp
    private int HexToInt(char hexChar)
    {
        hexChar = char.ToUpper(hexChar);

        return (int)hexChar < (int)'A' ?
            ((int)hexChar - (int)'0') :
            10 + ((int)hexChar - (int)'A');
    }

    private Token? ConsumePunctuator()
    {
        int startPosition = _position;
        
        foreach (ConstToken token in Punctuators)
        {
            if (_position + token.Text.Length > _fileText.Length)
                continue;
            
            if (_fileText.IndexOf(token.Text, _position, token.Text.Length, StringComparison.Ordinal) != _position)
                continue;

            if (token.Value == ConstTokenValue.Hash)
                _preprocessorLine = true;
            
            _position += token.Text.Length;
            return new Token()
            {
                preprocessor = _preprocessorLine,
                type = TokenType.Punctuator,
                text = token.Text,
                lineNumber = _lineNumber,
                characterNumber = startPosition,
                constantTokenValue = token.Value,
                skippable = token.Skippable
            };
        }

        return null;
    }
    
    private Token? ConsumeKeyword()
    {
        int startPosition = _position;
        
        foreach (ConstToken token in Keywords)
        {
            if (_position + token.Text.Length > _fileText.Length)
                continue;
            
            if (_fileText.IndexOf(token.Text, _position, token.Text.Length, StringComparison.Ordinal) != _position)
                continue;

            _position += token.Text.Length;
            
            // Identifier starting with keyword
            if (!IsEof() && (IsNonDigit(_fileText[_position]) || IsDigit(_fileText[_position]) ||
                             ConsumeUniversalCharacterName() > 0))
            {
                _position = startPosition;
                continue;
            }
            
            return new Token()
            {
                preprocessor = false,
                type = TokenType.Keyword,
                text = token.Text,
                lineNumber = _lineNumber,
                characterNumber = startPosition,
                constantTokenValue = token.Value,
                skippable = token.Skippable
            };
        }

        return null;
    }

    private Token? ConsumeConstant()
    {
        // Float must be checked before integer
        return ConsumeFloatConstant() ?? ConsumeIntegerConstant() ?? ConsumeCharacterConstant();
    }

    private Token? ConsumeIntegerConstant()
    {
        int startPosition = _position;
        double? intergerValue = null;
        
        if (_fileText[_position] is '0' && (_fileText[_position + 1] is 'x' or 'X'))
        {
            //! Hexadecimal constant
            _position += 2;

            while (IsHexadecimalDigit(_fileText[_position]))
            {
                _position++;
            }
            
            intergerValue = Convert.ToUInt64(_fileText.Substring(startPosition, _position - startPosition), 16);
        } 
        else if (_fileText[_position] == '0')
        {
            //! Octal constant
            while (IsOctalDigit(_fileText[_position]))
            {
                _position++;
            }
            
            intergerValue = Convert.ToUInt64(_fileText.Substring(startPosition, _position - startPosition), 8);
        }
        else if (IsNonZeroDigit(_fileText[_position]))
        {
            //! Decimal constant
            while (IsDigit(_fileText[_position]))
            {
                _position++;
            }
            
            intergerValue = Convert.ToUInt64(_fileText.Substring(startPosition, _position - startPosition), 10);
        }
        else
        {
            // Not integer
            return null;
        }
        
        //! Integer suffix
        if (_fileText[_position] is 'u' or 'U')
        {
            // ull
            _position++;

            if (_fileText[_position] is 'l' or 'L')
                _position++;
            
            // ll or LL not Ll
            if (_fileText[_position - 1] == 'l' && _fileText[_position] == 'l')
                _position++;
            
            if (_fileText[_position - 1] == 'L' && _fileText[_position] == 'L')
                _position++;
        }
        else if (_fileText[_position] is 'l' or 'L')
        {
            // llu
            _position++;
            
            // ll or LL not Ll
            if (_fileText[_position - 1] == 'l' && _fileText[_position] == 'l')
                _position++;
            
            if (_fileText[_position - 1] == 'L' && _fileText[_position] == 'L')
                _position++;
            
            if (_fileText[_position] is 'u' or 'U')
                _position++;
        }
        
        return new Token()
        {
            preprocessor = _preprocessorLine,
            type = TokenType.IntegerConstant,
            text = _fileText.Substring(startPosition, _position - startPosition),
            constantValue = intergerValue,
            lineNumber = _lineNumber,
            characterNumber = startPosition
        };
    }

    private (int, long) ConsumeExponent()
    {
        int startPosition = _position;
        
        if (_fileText[_position] is not 'e' or 'E')
            return (0, 0);

        _position++;
        
        // Optional sign
        char sign = '+';
        if (_fileText[_position] is '+' or '-')
        {
            sign = _fileText[_position];
            _position++;
        }

        if (!IsDigit(_fileText[_position]))
            goto error;

        // Digit sequence
        int exponentStart = _position;
        while (IsDigit(_fileText[_position]))
            _position++;

        long exponent = Convert.ToInt64(_fileText.Substring(exponentStart, _position - exponentStart));
        exponent *= sign == '+' ? 1L : -1L;

        return (_position - startPosition, exponent);
        
        error:
        _position = startPosition;
        return (0, 0);
    }

    private Token? ConsumeFloatConstant()
    {
        int startPosition = _position;
        double? floatValue = null;

        if (_fileText[_position] is '0' && (_fileText[_position + 1] is 'x' or 'X'))
        {
            //! Hexadecimal floating constant
            _position += 2;

            // Hexadecimal digit sequence
            while (IsHexadecimalDigit(_fileText[_position]))
                _position++;

            string integral = _fileText.Substring(startPosition + 2, _position - startPosition - 2);
            floatValue = integral != "" ? Convert.ToUInt64(integral, 16) : 0;
            
            int pointPos = _position;
            if (_fileText[_position] == '.')
                _position++;

            // Hexadecimal digit sequence
            int positionAfterPoint = 1;
            while (IsHexadecimalDigit(_fileText[_position]))
            {
                floatValue += HexToInt(_fileText[_position]) * (double) Math.Pow(16, -positionAfterPoint);
                positionAfterPoint++;
                
                _position++;
            }

            // Binary exponent part
            if (_fileText[_position] is 'p' or 'P')
                _position++;
            else
                goto error;
            
            // Optional sign
            char sign = '+';
            if (_fileText[_position] is '+' or '-')
            {
                sign = _fileText[_position];
                _position++;
            }

            if (!IsDigit(_fileText[_position]))
                goto error;

            int exponentStart = _position;
            while (IsDigit(_fileText[_position]))
                _position++;

            long exponent = Convert.ToInt64(_fileText.Substring(exponentStart, _position - exponentStart));
            exponent *= sign == '+' ? 1L : -1L;
            floatValue *= (double) Math.Pow(2, exponent);
        }
        else if (IsDigit(_fileText[_position]) || _fileText[_position] is '.')
        {
            //! Decimal floating constant
            
            // Digit sequence
            while (IsDigit(_fileText[_position]))
                _position++;

            string integral = _fileText.Substring(startPosition, _position - startPosition);
            floatValue = integral != "" ? Convert.ToUInt64(integral, 10) : 0;

            // Must have . or exponent
            long exponent;
            if (_fileText[_position] is '.')
                _position++;
            else
            {
                int consumed;
                (consumed, exponent) = ConsumeExponent();

                floatValue *= (double) Math.Pow(10, exponent);
                
                if (consumed > 0)
                    goto floating_suffix;
                
                goto error;
            }

            // Digit sequence
            int positionAfterPoint = 1;
            while (IsDigit(_fileText[_position]))
            {
                floatValue += char.GetNumericValue(_fileText[_position])* Math.Pow(10, -positionAfterPoint);
                _position++;
            }

            // Optional exponent
            (_, exponent) = ConsumeExponent();
            floatValue *= (double) Math.Pow(10, exponent);
        }
        else
        {
            goto error;
        }

        floating_suffix:
        if (_fileText[_position] is 'f' or 'F' or 'l' or 'L')
            _position++;
        
        return new Token()
        {
            preprocessor = _preprocessorLine,
            type = TokenType.FloatConstant,
            text = _fileText.Substring(startPosition, _position - startPosition),
            constantValue = floatValue,
            lineNumber = _lineNumber,
            characterNumber = startPosition
        };
        
        error:
        _position = startPosition;
        return null;
    }

    private int ConsumeEscapeSequence()
    {
        int startPosition = _position;
        
        if (_fileText[_position] is not '\\')
            return 0;

        _position++;
        
        // simple-escape-sequence
        if (_fileText[_position] is '\'' or '"' or '?' or '\\' or 'a' or 'b' or 'f' or 'n' or 'r' or 't' or 'v')
        {
            _position++;
            return _position - startPosition;
        }

        // Octal escape sequence
        if (IsOctalDigit(_fileText[_position]))
        {
            for (int i = 0; i < 3; i++)
            {
                _position++;
                if (!IsOctalDigit(_fileText[_position]))
                    return _position - startPosition;
            }
        }
        
        // Hexadecimal escape sequence
        if (_fileText[_position] is 'x')
        {
            _position++;

            if (!IsHexadecimalDigit(_fileText[_position]))
                throw new LexerException("Invalid hexadecimal escape sequence", _filePath, _lineNumber, _position);
            
            while (IsHexadecimalDigit(_fileText[_position]))
                _position++;
            
            return _position - startPosition;
        }

        // Universal character name
        _position = startPosition; // ConsumeUniversalCharacterName starts with \
        return ConsumeUniversalCharacterName();
    }

    private Token? ConsumeCharacterConstant()
    {
        int startPosition = _position;
        
        if (_fileText[_position] is 'L' or 'U' or 'u')
            _position++;

        if (_fileText[_position] is not '\'')
        {
            _position = startPosition;
            return null;
        }
        
        // Opening '
        _position++;
        int charactersStart = _position;
        
        if (!IsEof() && _fileText[_position] == '\'')
            throw new LexerException("Empty character constant", _filePath, _lineNumber, _position);

        int escapedSequenceLen = 0;
        while (!IsEof() && (_fileText[_position] is not '\\' and not '\'' and not '\n' || (escapedSequenceLen = ConsumeEscapeSequence()) > 0))
        {
            if (escapedSequenceLen == 0)
                _position++;

            escapedSequenceLen = 0;
        }
        
        if (IsEof())
            throw new LexerException("End of file in character constant", _filePath, _lineNumber, _position);

        if (_fileText[_position] is '\n')
            throw new LexerException("New line in character constant", _filePath, _lineNumber, _position);

        ulong? value = 0;
        try
        {
            string characters = _fileText.Substring(charactersStart, _position - charactersStart);
            characters = Regex.Unescape(characters);
            byte[] bytes = Encoding.UTF8.GetBytes(characters);
            
            for (int i = 0; i < bytes.Length && i < 8; i++)
            {
                value = value | ((bytes[i] & 0xFFul) << i * 8);
            }
        }
        catch (ArgumentException)
        {
            value = null;
        }
        
        // Closing '
        _position++;
        
        return new Token()
        {
            preprocessor = _preprocessorLine,
            type = TokenType.CharacterConstant,
            text = _fileText.Substring(startPosition, _position - startPosition),
            lineNumber = _lineNumber,
            characterNumber = startPosition,
            constantValue = value
        };
    }

    private Token? ConsumeString()
    {
        int startPosition = _position;

        if (_fileText[_position] is 'L' or 'U')
            _position++;
        else if (_fileText[_position] == 'u')
        {
            _position++;

            if (_fileText[_position] == '8')
                _position++;
        }

        if (_fileText[_position] is not '"')
        {
            _position = startPosition;
            return null;
        }
        
        // Opening "
        _position++;
        
        int escapedSequenceLen = 0;
        while (!IsEof() && (_fileText[_position] is not '\\' and not '"' and not '\n' || (escapedSequenceLen = ConsumeEscapeSequence()) > 0))
        {
            if (escapedSequenceLen == 0)
                _position++;

            escapedSequenceLen = 0;
        }
        
        if (IsEof())
            throw new LexerException("End of file in string", _filePath, _lineNumber, _position);

        if (_fileText[_position] is '\n')
            throw new LexerException("New line in string", _filePath, _lineNumber, _position);

        // Closing "
        _position++;
        
        return new Token()
        {
            preprocessor = _preprocessorLine,
            type = TokenType.String,
            text = _fileText.Substring(startPosition, _position - startPosition),
            lineNumber = _lineNumber,
            characterNumber = startPosition
        };
    }

    private int ConsumeUniversalCharacterName()
    {
        if (_fileText[_position] is not '\\' || (_fileText[_position + 1] is not 'u' and not 'U'))
            return 0;
        
        _position += 2;

        int numHexDigits = _fileText[_position - 1] is 'u' ? 4 : 8;
        for (int i = 0; i < numHexDigits; i++)
        {
            if (!IsHexadecimalDigit(_fileText[_position]))
                throw new LexerException("Invalid universal character name", _filePath, _lineNumber, _position);

            _position++;
        }
        
        return numHexDigits + 2;
    }

    private Token? ConsumeIdentifier()
    {
        int startPosition = _position;
        
        int numUniversalConsumed = 0;
        if (!IsNonDigit(_fileText[_position]) && (numUniversalConsumed = ConsumeUniversalCharacterName()) == 0)
            return null;
        
        if (numUniversalConsumed == 0)
            _position++;

        while (!IsEof() && (IsNonDigit(_fileText[_position]) || IsDigit(_fileText[_position]) || (numUniversalConsumed = ConsumeUniversalCharacterName()) > 0))
        {
            if (numUniversalConsumed == 0)
                _position++;

            numUniversalConsumed = 0;
        }
        
        return new Token()
        {
            preprocessor = _preprocessorLine,
            type = TokenType.Identifier,
            text = _fileText.Substring(startPosition, _position - startPosition),
            lineNumber = _lineNumber,
            characterNumber = startPosition,
            constantTokenValue = ConstTokenValue.Identifier
        };
    }
    
    private Token? ConsumePreprocessorKeyword()
    {
        // Must be preceded by #
        if (!_preprocessorLine || _previousToken?.text != "#")
            return null;
        
        int startPosition = _position;
        
        foreach (ConstToken token in PreprocessorKeywords)
        {
            if (_position + token.Text.Length > _fileText.Length)
                continue;
            
            if (_fileText.IndexOf(token.Text, _position, token.Text.Length, StringComparison.Ordinal) != _position)
                continue;

            _position += token.Text.Length;
            
            // Identifier starting with keyword
            if (!IsEof() && (IsNonDigit(_fileText[_position]) || IsDigit(_fileText[_position]) ||
                             ConsumeUniversalCharacterName() > 0))
            {
                _position = startPosition;
                continue;
            }
            
            return new Token()
            {
                preprocessor = true,
                type = TokenType.PreprocessorKeyword,
                text = token.Text,
                lineNumber = _lineNumber,
                characterNumber = startPosition,
                constantTokenValue = token.Value,
                skippable = token.Skippable
            };
        }

        return null;
    }
    
    private Token? ConsumeHeaderName()
    {
        //TODO check is actual header name and not string
        /*
         * Can only appear in preprocessor directives but must replace to
         * # include <h-char-sequence> new-line
         * or
         * # include "q-char-sequence" new-line
         * so is valid
         *
         *  #if VERSION == 1
         *      #define INCFILE "vers1.h"
         *  #elif VERSION == 2
         *      #define INCFILE "vers2.h" // and so on
         *  #else
         *      #define INCFILE "versN.h"
         *  #endif
         *  #include INCFILE
         *
         * Maybe accept all strings in preprocessor directive get match as a header name???
         */
        if (!_preprocessorLine)
            return null;
        
        if (_fileText[_position] is not '<' and not '"')
            return null;

        int startPosition = _position;
        
        // < or "
        char endChar = _fileText[_position] == '<' ? '>' : '"';
        _position++;

        if (!IsEof() && _fileText[_position] == endChar)
        {
            _position = startPosition;
            return null;
        }

        while (!IsEof() && _fileText[_position] != '\n' && _fileText[_position] != endChar)
            _position++;

        if ((IsEof() || _fileText[_position] == '\n') && endChar == '>')
        {
            // Assume opening < was actually a less than symbol
            _position = startPosition;
            return null;
        }
        
        if (IsEof())
            throw new LexerException("End of file in header name", _filePath, _lineNumber, _position);

        if (_fileText[_position] == '\n')
            throw new LexerException("New line in header name", _filePath, _lineNumber, _position);

        // > or "
        _position++;
        
        return new Token()
        {
            preprocessor = true,
            type = TokenType.HeaderName,
            text = _fileText.Substring(startPosition, _position - startPosition),
            lineNumber = _lineNumber,
            characterNumber = startPosition
        };
    }

    private Token? ConsumePpNumber()
    {
        /*
         * Weird grammar allows for numbers such as
         * 0.0e+e+e+e+e+0.0e+
         */
        if (!_preprocessorLine)
            return null;
        
        if (!IsDigit(_fileText[_position]) && _fileText[_position] != '.')
            return null;

        int startPosition = _position;
        
        // Consume digit / .
        _position++;
        
        // If . first must be followed by digit
        if (_fileText[_position - 1] == '.' && !IsDigit(_fileText[_position]))
        {
            _position = startPosition;
            return null;
        }
        
        if (_fileText[_position - 1] == '.' && IsDigit(_fileText[_position]))
            _position++;

        int numUniversalConsumed = 0;
        while (!IsEof() && 
               (IsDigit(_fileText[_position]) || IsNonDigit(_fileText[_position]) || _fileText[_position] == '.' ||
                (numUniversalConsumed = ConsumeUniversalCharacterName()) > 0 ||
                (_fileText[_position - 1] is 'e' or 'E' or 'p' or 'P' && _fileText[_position] is '+' or '-')))
        {
            if (numUniversalConsumed == 0)
                _position++;
            
            numUniversalConsumed = 0;
        }

        return new Token()
        {
            preprocessor = true,
            type = TokenType.PpNumber,
            text = _fileText.Substring(startPosition, _position - startPosition),
            lineNumber = _lineNumber,
            characterNumber = startPosition
        };
    }
    
    private int ConsumeSingleLineComment()
    {
        /*
         * Terminating new line character can be escaped making comments spread across multiple lines
         * such as
         *
         * // Fist line of comment \
         * f(); second line \
         * third and last line
         */
        if (_position + 1 >= _fileText.Length)
            return 0;
        
        if (_fileText[_position] != '/' || _fileText[_position + 1] != '/')
            return 0;

        int startPosition = _position;
        _position += 2;

        while (!IsEof() && (_fileText[_position - 1] == '\\' || _fileText[_position] != '\n'))
        {
            // Must be escaped new line
            if (_fileText[_position] == '\n')
                _consecutiveEscapedNewLines++;

            _position++;
        }

        return _position - startPosition;
    }

    private int ConsumeMultilineComment()
    {
        /*
         * Preprocessor are normally terminated by a new line but can contain multiline comments within them
         * such as
         *
         * 1| #define MY_NUMBER /* my
         * 2| comment
         * 3| line * / 5
         * 4|
         * 5| func();
         * 
         * Handled by "5" being on line 1 then incrementing the line number by 2
         * on the first new line outside the comment
         */
        if (_position + 1 >= _fileText.Length)
            return 0;
        
        if (_fileText[_position] != '/' || _fileText[_position + 1] != '*')
            return 0;

        int startPosition = _position;
        _position += 2;

        while (!IsEof() && !(_fileText[_position - 1] == '*' && _fileText[_position] == '/'))
        {
            if (_fileText[_position] == '\n')
                _consecutiveEscapedNewLines++;
            
            _position++;
        }
        
        // /
        _position++;

        return _position - startPosition;
    }

    private int ConsumeEscapedNewLine()
    {
        int startPosition = _position;

        while (_position + 1 < _fileText.Length && _fileText[_position] == '\\' && _fileText[_position + 1] == '\n')
        {
            _position += 2;
            _consecutiveEscapedNewLines++;
        }

        return _position - startPosition;
    }

    private int ConsumeWsCommentsNewlines()
    {
        int startPosition = _position;

        int numConsumed = 0;
        while (!IsEof() && (_fileText[_position] is ' ' or '\r' or '\n' or '\t' || 
                            (numConsumed = ConsumeSingleLineComment()) > 0 || 
                            (numConsumed = ConsumeMultilineComment()) > 0 ||
                            (numConsumed = ConsumeEscapedNewLine()) > 0))
        {
            if (numConsumed == 0)
            {
                if (_fileText[_position] == '\n')
                {
                    _lineNumber++;
                    _preprocessorLine = false;
                    
                    // Add all escaped / newlines in multiline comments
                    _lineNumber += _consecutiveEscapedNewLines;
                    _consecutiveEscapedNewLines = 0;
                }
                
                _position++;
            }

            numConsumed = 0;
        }

        return _position - startPosition;
    }
    
    private Token? GetNextToken()
    {
        ConsumeWsCommentsNewlines();
        
        // End of file
        if (IsEof())
        {
            return null;
        }
        
        // Order is important (eg identifier would match keywords)
        // Punctuators would match opening < in header name but header name matches <: punctuator :(
        GetTokenMethod[] tokenMethods = new GetTokenMethod[]
        {
            ConsumeHeaderName,
            ConsumeConstant,
            ConsumePunctuator,
            ConsumePreprocessorKeyword,
            ConsumeKeyword,
            ConsumePpNumber,
            ConsumeString,
            ConsumeIdentifier
        };
        
        foreach (var tokenMethod in tokenMethods)
        {
            Token? t = tokenMethod();
            if (t != null)
                return t;
        }

        throw new LexerException("No token found", _filePath, _lineNumber, _position);
    }

    public List<Token> GetAllTokens()
    {
        Token? t;
        while ((t = GetNextToken()) != null)
        {
            _tokens.Add(t);
            _previousToken = t;
        }

        return _tokens;
    }

    public List<Token> GetParsedTokens()
    {
        return _tokens;
    }

    public CFileInfo GetFileInfo()
    {
        return new CFileInfo()
        {
            NumCharacters = _fileText.Length,
            NumLines = _totalNumberLines
        };
    }
}