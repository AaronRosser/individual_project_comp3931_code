namespace CTools;

public class LexerException : Exception
{
    public LexerException()
    {
    }
    
    public LexerException(string message, string fileName, int lineNumber, int position)
        : base($"{message} - {fileName} - Line {lineNumber} - Position {position}")
    {
    }

    public LexerException(string message)
        : base(message)
    {
    }

    public LexerException(string message, Exception inner)
        : base(message, inner)
    {
    }
}