using CTools.Lexer;

namespace CTools;

public class Parser
{
    public static void MarkAllSkippableBrackets(List<Token> tokens)
    {
        for (int position = 0; position < tokens.Count; position++)
        {
            // Increments on true return leading to skipped token in next iteration
            MarkScope(tokens, ref position);
        }
    }

    private static void MarkScope(List<Token> tokens, ref int position)
    {
        if (position >= tokens.Count || (tokens[position].type == TokenType.Punctuator && 
                                         tokens[position].constantTokenValue != ConstTokenValue.OpenBrace))
            return;

        int startPosition = position;
        int numScopes = 0;
        int numSemiColons = 0;
        
        while (position < tokens.Count)
        {
            switch (tokens[position].type)
            {
                case TokenType.Punctuator when tokens[position].constantTokenValue == ConstTokenValue.Semicolon:
                    numSemiColons++;
                    break;
                case TokenType.Punctuator when tokens[position].constantTokenValue == ConstTokenValue.OpenBrace:
                    numScopes++;
                    MarkScope(tokens, ref position);
                    break;
                case TokenType.Punctuator when tokens[position].constantTokenValue == ConstTokenValue.CloseBrace:
                {
                    if (numSemiColons + numScopes <= 1)
                    {
                        tokens[startPosition].skippable = true;
                        tokens[position].skippable = true;
                    }
                    return;
                }
            }

            position++;
        }
        
        // Bracket never closed
    }
}