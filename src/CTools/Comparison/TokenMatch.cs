namespace CTools.Comparison;

public class TokenMatch
{
    public int FileATokenIndexStart { get; set; }
    public int FileBTokenIndexStart { get; set; }
    
    public int NumberMatchingTokens { get; set; }
}