using System.Collections;

namespace CTools.Comparison;

public static class TokenComparer
{
    private const int MinimumMatchLength = 25;
    private static int GetMatchPosition(int bLength, int i, int j) => i * bLength + j;
    public static List<TokenMatch> CompareFileTokens(IList<Token> a, IList<Token> b)
    {
        List<TokenMatch> res = new List<TokenMatch>();
        
        int numComparisons = a.Count * b.Count;
        var tokenMatches = new BitArray(numComparisons);
        
        // Compare every token in a with every token in b
        for (int i = 0; i < a.Count; i++)
        {
            for (int j = 0; j < b.Count; j++)
            {
                int matchPosition = GetMatchPosition(b.Count, i, j);
                
                // Match identifiers with different names, keywords and punctuators
                if (a[i].constantTokenValue is not null && a[i].constantTokenValue == b[j].constantTokenValue)
                {
                    tokenMatches[matchPosition] = true;
                    continue;
                }
                
                // Match constants formatted differently
                if (a[i].constantValue is not null && b[j].constantValue is not null &&
                    Math.Abs((a[i].constantValue ?? 0) - (b[j].constantValue ?? 0)) < 0.0001)
                {
                    tokenMatches[matchPosition] = true;
                    continue;
                }
                
                // Match identical strings, header names and pp numbers
                if (a[i].type == b[j].type && a[i].text == b[j].text)
                {
                    tokenMatches[matchPosition] = true;
                }
            }
        }
        
        // Search for matches
        for (int i = 0; i < a.Count; i++)
        {
            for (int j = 0; j < b.Count; j++)
            {
                int matchPosition = GetMatchPosition(b.Count, i, j);
                if (!tokenMatches[matchPosition])
                    continue;
                
                int nextI = i + 1;
                int nextJ = j + 1;
                while ((matchPosition = GetMatchPosition(b.Count, nextI, nextJ)) < numComparisons && nextJ < b.Count && tokenMatches[matchPosition])
                {
                    // Un-match to stop re-traversal
                    tokenMatches[GetMatchPosition(b.Count, nextI, nextJ)] = false;
                    
                    // Skip single statement scopes and irrelevant keywords
                    while (nextI < a.Count - 1 && a[nextI + 1].skippable)
                        nextI++;
                    while (nextJ < b.Count - 1 && b[nextJ + 1].skippable)
                        nextJ++;
                    
                    // Move to diagonal
                    nextI++;
                    nextJ++;
                }

                if (nextI - i >= MinimumMatchLength)
                {
                    res.Add(new TokenMatch()
                    {
                        FileATokenIndexStart = i,
                        FileBTokenIndexStart = j,
                        NumberMatchingTokens = Math.Max(nextI - i, nextJ - j)
                    });
                }
            }
        }
        
        return res;
    }
}