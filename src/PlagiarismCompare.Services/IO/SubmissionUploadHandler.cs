using System.IO.Compression;
using CTools;
using CTools.Lexer;
using Microsoft.AspNetCore.Hosting;
using PlagiarismCompare.Data;
using PlagiarismCompare.Data.Models;


namespace PlagiarismCompare.Services.IO;

public class SubmissionUploadHandler
{
    private readonly DataContext _context;
    private readonly IWebHostEnvironment _hostEnvironment;
    private const string BaseDirectory = "uploads/submissions/";

    public SubmissionUploadHandler(DataContext context, IWebHostEnvironment hostEnvironment)
    {
        _context = context;
        _hostEnvironment = hostEnvironment;
    }
    public async Task<Submission> ParseSubmission(string submissionName, string fileNamePrefix, Stream fileStream, SemesterStudent semesterStudent)
    {
        // Save zip file
        Guid id = Guid.NewGuid();
        string fileNameWithoutExtension = $"{fileNamePrefix}_{id}";
        string filePath = Path.Join(_hostEnvironment.WebRootPath, BaseDirectory, $"{fileNameWithoutExtension}.zip");

        await using (FileStream s = new FileStream(filePath, FileMode.Create))
        {
            await fileStream.CopyToAsync(s);
        }

        string extractPath = Path.Join(_hostEnvironment.WebRootPath, BaseDirectory, $"{fileNameWithoutExtension}/");
        
        // Extract zip file
        List<string> cFilePaths = ExtractCFiles(filePath, extractPath);
        
        // Add submission to database
        DateTime parsingStart = DateTime.Now;
        Submission submission = new Submission()
        {
            Name = submissionName,
            FileNamePrefix = fileNameWithoutExtension,
            State = SubmissionState.Parsed,
            CFiles = ProcessCFiles(extractPath, cFilePaths),
            ParsingStart = parsingStart,
            Submitted = DateTime.Now,
            SemesterStudent = semesterStudent
        };
        _context.Submissions.Add(submission);
        await _context.SaveChangesAsync();

        return submission;
    }

    private List<CFile> ProcessCFiles(string basePath, List<string> cFilePaths)
    {
        List<CFile> files = new List<CFile>();
        
        foreach (var filePath in cFilePaths)
        {
            string fullFilePath = Path.GetFullPath(Path.Combine(basePath, filePath));
            Lexer lexer = new Lexer(fullFilePath);

            try
            {
                var tokens = lexer.GetAllTokens();
                var fileInfo = lexer.GetFileInfo();
                files.Add(new CFile()
                {
                    FilePath = filePath,
                    Tokens = lexer.GetAllTokens(),
                    NumCharacters = fileInfo.NumCharacters,
                    NumLines = fileInfo.NumLines,
                    NumTokens = tokens.Count
                });
            }
            catch (LexerException e)
            {
                var tokens = lexer.GetParsedTokens();
                var fileInfo = lexer.GetFileInfo();
                files.Add(new CFile()
                {
                    FilePath = filePath,
                    Tokens = lexer.GetAllTokens(),
                    NumCharacters = fileInfo.NumCharacters,
                    NumLines = fileInfo.NumLines,
                    NumTokens = tokens.Count,
                    Error = e.Message
                });
            }
        }

        return files;
    }

    private List<string> ExtractCFiles(string filePath, string extractPath)
    {
        // From
        // https://docs.microsoft.com/en-us/dotnet/standard/io/how-to-compress-and-extract-files
        
        // Prevent path traversal
        if (!extractPath.EndsWith(Path.DirectorySeparatorChar.ToString(), StringComparison.Ordinal))
        {
            extractPath += Path.DirectorySeparatorChar;
        }

        List<string> cFilePaths = new List<string>();

        using ZipArchive archive = ZipFile.OpenRead(filePath);
        foreach (ZipArchiveEntry entry in archive.Entries)
        {
            if (!entry.FullName.EndsWith(".c", StringComparison.OrdinalIgnoreCase))
            {
                continue;
            }
            
            // Gets the full path to ensure that relative segments are removed.
            string destinationPath = Path.GetFullPath(Path.Combine(extractPath, entry.FullName));

            // Ordinal match is safest, case-sensitive volumes can be mounted within volumes that
            // are case-insensitive.
            if (destinationPath.StartsWith(extractPath, StringComparison.Ordinal))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(destinationPath) ?? "");
                entry.ExtractToFile(destinationPath);
                cFilePaths.Add(entry.FullName);
            }
        }

        return cFilePaths;
    }
}