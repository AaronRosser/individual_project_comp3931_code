using Microsoft.EntityFrameworkCore;
using PlagiarismCompare.Data;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Services;

public class SubmissionComparisonHandler
{
    private readonly DataContext _context;
    
    public SubmissionComparisonHandler(DataContext context)
    {
        _context = context;
    }

    public async Task RunComparisons(Submission submission)
    {
        var submissions = await _context.Submissions
            .Where(s => s.Id < submission.Id)
            .Include(s => s.CFiles)
            .ToListAsync();
        
        submission.ComparisonStart = DateTime.Now;
        foreach (var otherSubmission in submissions)
        {
            submission.CompareSubmissions(otherSubmission);
        }
        submission.ComparisonEnd = DateTime.Now;
        submission.ComparisonSeconds = (submission.ComparisonEnd - submission.ComparisonStart)?.TotalSeconds;

        submission.State = SubmissionState.Processed;
        
        // Manually update due to loss of tracking from background service
        _context.Update(submission);
        await _context.SaveChangesAsync();
    }
}