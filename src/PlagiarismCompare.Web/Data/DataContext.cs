using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Web.Data;

// public class DataContext : IdentityDbContext<User>
// {
//     public DataContext(DbContextOptions<DataContext> options) : base(options)
//     {
//     }
//
//     public DbSet<Student> Students { get; set; }
//     
//     public DbSet<Module> Modules { get; set; }
//     public DbSet<Semester> ModuleSemesters { get; set; }
//     
//     public DbSet<SemesterStudent> SemesterStudents { get; set; }
//     
//     public DbSet<Submission> Submissions { get; set; }
//     public DbSet<CFile> CFiles { get; set; }
//     public DbSet<ComparisonResult> ComparisonResults { get; set; }
// }