#nullable disable
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlagiarismCompare.Services;
using PlagiarismCompare.Services.IO;
using PlagiarismCompare.Data;
using PlagiarismCompare.Data.Models;
using PlagiarismCompare.Web.BackgroundServices;
using PlagiarismCompare.Web.Models;

namespace PlagiarismCompare.Web.Controllers;

[Authorize]
public class SubmissionController : Controller
{
    private readonly DataContext _context;
    private readonly SubmissionUploadHandler _uploadHandler;
    private readonly SubmissionComparisonHandler _comparisonHandler;
    private readonly SubmissionComparisonQueue _comparisonQueue;

    public SubmissionController(DataContext context, SubmissionUploadHandler uploadHandler, SubmissionComparisonHandler comparisonHandler, SubmissionComparisonQueue comparisonQueue)
    {
        _context = context;
        _uploadHandler = uploadHandler;
        _comparisonHandler = comparisonHandler;
        _comparisonQueue = comparisonQueue;
    }

    // GET: Submission
    public async Task<IActionResult> Index()
    {
        return View(await _context.Submissions
            .Include(s => s.SemesterStudent)
            .ThenInclude(s => s.Semester)
            .ThenInclude(s=> s.Module)
            .Include(s => s.SemesterStudent)
            .ThenInclude(s => s.Student)
            .ToListAsync());
    }

    // GET: Submission/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var submission = await _context.Submissions
            .Include(s => s.SemesterStudent)
            .ThenInclude(s => s.Semester)
            .ThenInclude(s=> s.Module)
            .Include(s => s.SemesterStudent)
            .ThenInclude(s => s.Student)
            .Include(s => s.CFiles)
            .ThenInclude(c => c.ComparisonResults)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (submission == null)
        {
            return NotFound();
        }

        return View(submission);
    }

    // GET: Submission/Create/5
    public async Task<IActionResult> Create(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var semesterStudent = await _context.SemesterStudents
            .Include(s=> s.Student)
            .Include(s=> s.Semester)
            .ThenInclude(s=> s.Module)
            .FirstOrDefaultAsync(s => s.Id == id);
        if (semesterStudent == null)
        {
            return NotFound();
        }

        ViewBag.SemesterStudent = semesterStudent;
        return View(new SubmissionForm());
    }

    // POST: Submission/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(int id, [Bind("Name,ZipFile")] SubmissionForm submissionForm)
    {
        var semesterStudent = await _context.SemesterStudents
            .Include(s=> s.Student)
            .Include(s=> s.Semester)
            .ThenInclude(s=> s.Module)
            .FirstOrDefaultAsync(s => s.Id == id);
        if (semesterStudent == null)
        {
            return NotFound();
        }
        
        if (!ModelState.IsValid)
        {
            ViewBag.SemesterStudent = semesterStudent;
            return View(submissionForm);
        }
        
        string fileNamePrefix = $"{semesterStudent.Student.Code}_{semesterStudent.Semester.Module.Code}";
        var submission = await _uploadHandler.ParseSubmission(submissionForm.Name, fileNamePrefix, submissionForm.ZipFile.OpenReadStream(),
            semesterStudent);

        // Compare submission with all other submissions
        _comparisonQueue.Enqueue(submission);

        return RedirectToAction(nameof(Details), new {Id = submission.Id});
    }

    // GET: Submission/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var submission = await _context.Submissions.FirstOrDefaultAsync(m => m.Id == id);
        if (submission == null)
        {
            return NotFound();
        }

        return View(submission);
    }

    // POST: Submission/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var submission = await _context.Submissions
            .Include(s => s.CFiles)
            .ThenInclude(c => c.ComparisonResults)
            .FirstOrDefaultAsync(s => s.Id == id);
        
        if (submission == null)
        {
            return NotFound();
        }
        
        foreach (var cFile in submission.CFiles) {
            foreach (var comparisonResult in cFile.ComparisonResults)
            {
                _context.ComparisonResults.Remove(comparisonResult);
            }
        }
        await _context.SaveChangesAsync();

        _context.Submissions.Remove(submission);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }
}