using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlagiarismCompare.Data;
using PlagiarismCompare.Data.Models;
using PlagiarismCompare.Web.Models;

namespace PlagiarismCompare.Web.Controllers;

[Authorize]
public class SemesterStudentController : Controller
{
    private readonly DataContext _context;

    public SemesterStudentController(DataContext context)
    {
        _context = context;
    }
    
    public async Task<IActionResult> CreateFromStudent(int id)
    {
        var student = await _context.Students.FindAsync(id);
        if (student == null)
        {
            return NotFound();
        }

        var possibleSemesters = _context.ModuleSemesters
            .Include(s => s.Module)
            .Where(s => s.SemesterStudents.All(x => x.Student.Id != student.Id));

        ViewBag.student = student;
        ViewBag.possibleSemesters = possibleSemesters;
        return View();
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> CreateFromStudent(int id, [Bind("SemesterId")] SemesterStudentCreateForm semesterStudentForm)
    {
        var student = await _context.Students.FindAsync(id);
        if (student == null)
        {
            return NotFound();
        }

        var possibleSemesters = _context.ModuleSemesters
            .Include(s => s.Module)
            .Where(s => s.SemesterStudents.All(x => x.Student.Id != student.Id));
        
        var semester = possibleSemesters.FirstOrDefault(s => s.Id == semesterStudentForm.SemesterId);
        if (semester == null)
        {
            ModelState.AddModelError("SemesterId", "Invalid semester");
        }
        
        ModelState.Remove("StudentId");
        if (!ModelState.IsValid || semester == null)
        {
            ViewBag.student = student;
            ViewBag.possibleSemesters = possibleSemesters;
            return View(semesterStudentForm);
        }

        student.SemesterStudents.Add(new SemesterStudent()
        {
            Semester = semester
        });
        await _context.SaveChangesAsync();
        
        return RedirectToAction(nameof(StudentController.Details), "Student",new {id = student.Id});
    }
    
    public async Task<IActionResult> CreateFromSemester(int id)
    {
        var semester = await _context.ModuleSemesters
            .Include(s => s.Module)
            .FirstOrDefaultAsync(s => s.Id == id);
        if (semester == null)
        {
            return NotFound();
        }

        var possibleStudents = _context.Students.AsQueryable()
            .Where(s => s.SemesterStudents.All(x => x.Semester.Id != semester.Id));

        ViewBag.semester = semester;
        ViewBag.possibleStudents = possibleStudents;
        return View();
    }
    
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> CreateFromSemester(int id, [Bind("StudentId")] SemesterStudentCreateForm semesterStudentForm)
    {
        var semester = await _context.ModuleSemesters
            .Include(s => s.Module)
            .FirstOrDefaultAsync(s => s.Id == id);
        if (semester == null)
        {
            return NotFound();
        }

        var possibleStudents = _context.Students.AsQueryable().Where(s => s.SemesterStudents.All(x => x.Semester.Id != id));
        var student = possibleStudents.FirstOrDefault(s => s.Id == semesterStudentForm.StudentId);
        if (student == null)
        {
            ModelState.AddModelError("StudentId", "Invalid student");
        }
        
        ModelState.Remove("SemesterId");
        if (!ModelState.IsValid)
        {
            ViewBag.semester = semester;
            ViewBag.possibleStudents = possibleStudents;
            return View(semesterStudentForm);
        }

        semester.SemesterStudents.Add(new SemesterStudent()
        {
            Student = student!
        });
        await _context.SaveChangesAsync();
        
        return RedirectToAction(nameof(SemesterController.Details), "Semester",new {id = semester.Id});
    }
    
    // GET: SemesterStudent/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var semesterStudent = await _context.SemesterStudents
            .Include(s => s.Student)
            .Include(s => s.Semester)
            .ThenInclude(s => s.Module)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (semesterStudent == null)
        {
            return NotFound();
        }

        return View(semesterStudent);
    }

    // POST: SemesterStudent/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var semesterStudent = await _context.SemesterStudents
            .Include(s => s.Student)
            .FirstOrDefaultAsync(s => s.Id == id);
        if (semesterStudent == null)
        {
            return NotFound();
        }
        
        _context.SemesterStudents.Remove(semesterStudent);
        await _context.SaveChangesAsync();
        
        return RedirectToAction(nameof(StudentController.Details), "Student", new {id = semesterStudent.Student.Id});
    }
}