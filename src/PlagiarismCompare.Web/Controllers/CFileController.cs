using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using PlagiarismCompare.Data;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Web.Controllers;

[Authorize]
public class CFileController : Controller
{
    private readonly DataContext _context;

    public CFileController(DataContext context)
    {
        _context = context;
    }
    
    // GET
    public async Task<IActionResult> Index()
    {
        return View(await _context.CFiles
            .Include(c => c.Submission)
            .ThenInclude(s => s.SemesterStudent)
            .ThenInclude(s => s.Semester)
            .ThenInclude(s => s.Module)
            .Include(c => c.Submission)
            .ThenInclude(s => s.SemesterStudent)
            .ThenInclude(s => s.Student)
            .ToListAsync());
    }
    
    // GET: CFile/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var cFile = await _context.CFiles
            // Main file module
            .Include(c => c.Submission)
            .ThenInclude(s => s.SemesterStudent)
            .ThenInclude(s => s.Semester)
            .ThenInclude(s=> s.Module)
            // Main file student
            .Include(c => c.Submission)
            .ThenInclude(s => s.SemesterStudent)
            .ThenInclude(s => s.Student)
            .FirstOrDefaultAsync(m => m.Id == id);

        if (cFile == null)
        {
            return NotFound();
        }
        
        // Get 50 top results
        IQueryable<ComparisonResult> comparisonResults = _context.ComparisonResults
            // Other file module
            .Include(c => c.CFiles)
            .ThenInclude(c => c.Submission)
            .ThenInclude(s => s.SemesterStudent)
            .ThenInclude(s => s.Semester)
            .ThenInclude(s => s.Module)
            // Other file student
            .Include(c => c.CFiles)
            .ThenInclude(c => c.Submission)
            .ThenInclude(s => s.SemesterStudent)
            .ThenInclude(s => s.Student)
            .Where(r => r.CFiles.Contains(cFile) && r.LongestTokenMatch > 0)
            .Distinct()
            .OrderByDescending(r => r.LongestTokenMatch)
            .Take(50);

        ViewBag.ComparisonResults = comparisonResults;

        string reports = JsonConvert.SerializeObject(comparisonResults);
        ViewBag.Reports = reports;
        
        return View(cFile);
    }
}