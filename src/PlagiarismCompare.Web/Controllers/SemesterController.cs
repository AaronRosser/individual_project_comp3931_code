#nullable disable
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlagiarismCompare.Data;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Web.Controllers;

[Authorize]
public class SemesterController : Controller
{
    private readonly DataContext _context;

    public SemesterController(DataContext context)
    {
        _context = context;
    }

    // GET: Semester
    public async Task<IActionResult> Index()
    {
        return View(await _context.ModuleSemesters.Include(s => s.Module).ToListAsync());
    }

    // GET: Semester/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var semester = await _context.ModuleSemesters
            .Include(s => s.Module)
            .Include(s => s.SemesterStudents)
            .ThenInclude(s => s.Student)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (semester == null)
        {
            return NotFound();
        }

        return View(semester);
    }

    // GET: Semester/Create/5
    public async Task<IActionResult> Create(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var module = await _context.Modules.FirstOrDefaultAsync(m => m.Id == id);
        if (module == null)
        {
            return NotFound();
        }

        ViewBag.Module = module;
        return View();
    }

    // POST: Semester/Create/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create(int id, [Bind("Year,Term")] Semester semester)
    {
        var module = await _context.Modules.FirstOrDefaultAsync(m => m.Id == id);
        if (module == null)
        {
            return NotFound();
        }

        semester.Module = module;
        ModelState.Remove("Module");

        if (ModelState.IsValid)
        {
            _context.Add(semester);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
            
        ViewBag.Module = module;
        return View(semester);
    }

    // GET: Semester/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var semester = await _context.ModuleSemesters
            .Include(s => s.Module)
            .FirstOrDefaultAsync(s => s.Id == id);
            
        if (semester == null)
        {
            return NotFound();
        }
        return View(semester);
    }

    // POST: Semester/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Year,Term,Id")] Semester semester)
    {
        if (id != semester.Id)
        {
            return NotFound();
        }
            
        ModelState.Remove("Module");
        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(semester);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SemesterExists(semester.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }
            
        semester = await _context.ModuleSemesters
            .Include(s => s.Module)
            .FirstOrDefaultAsync(s => s.Id == id);
        return View(semester);
    }

    // GET: Semester/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var semester = await _context.ModuleSemesters.FirstOrDefaultAsync(m => m.Id == id);
        if (semester == null)
        {
            return NotFound();
        }

        return View(semester);
    }

    // POST: Semester/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var semester = await _context.ModuleSemesters.FindAsync(id);
        if (semester == null)
        {
            return NotFound();
        }
        
        _context.ModuleSemesters.Remove(semester);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool SemesterExists(int id)
    {
        return _context.ModuleSemesters.Any(e => e.Id == id);
    }
}