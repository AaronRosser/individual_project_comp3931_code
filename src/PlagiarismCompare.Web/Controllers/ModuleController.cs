#nullable disable
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PlagiarismCompare.Data;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Web.Controllers;

[Authorize]
public class ModuleController : Controller
{
    private readonly DataContext _context;

    public ModuleController(DataContext context)
    {
        _context = context;
    }

    // GET: Module
    public async Task<IActionResult> Index()
    {
        return View(await _context.Modules.ToListAsync());
    }

    // GET: Module/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var @module = await _context.Modules
            .Include(s=> s.Semesters)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (@module == null)
        {
            return NotFound();
        }

        return View(@module);
    }

    // GET: Module/Create
    public IActionResult Create()
    {
        return View();
    }

    // POST: Module/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Create([Bind("Code,Name,Id")] Module @module)
    {
        if (ModelState.IsValid)
        {
            _context.Add(@module);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        return View(@module);
    }

    // GET: Module/Edit/5
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var @module = await _context.Modules.FindAsync(id);
        if (@module == null)
        {
            return NotFound();
        }
        return View(@module);
    }

    // POST: Module/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> Edit(int id, [Bind("Code,Name,Id")] Module @module)
    {
        if (id != @module.Id)
        {
            return NotFound();
        }

        if (ModelState.IsValid)
        {
            try
            {
                _context.Update(@module);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModuleExists(@module.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return RedirectToAction(nameof(Index));
        }
        return View(@module);
    }

    // GET: Module/Delete/5
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null)
        {
            return NotFound();
        }

        var @module = await _context.Modules
            .FirstOrDefaultAsync(m => m.Id == id);
        if (@module == null)
        {
            return NotFound();
        }

        return View(@module);
    }

    // POST: Module/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        var @module = await _context.Modules.FindAsync(id);
        if (@module == null)
        {
            return NotFound();
        }
        
        _context.Modules.Remove(@module);
        await _context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool ModuleExists(int id)
    {
        return _context.Modules.Any(e => e.Id == id);
    }
}