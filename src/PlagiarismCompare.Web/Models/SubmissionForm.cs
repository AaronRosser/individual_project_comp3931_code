using System.ComponentModel.DataAnnotations;

namespace PlagiarismCompare.Web.Models;

public class SubmissionForm
{
    public string Name { get; set; } = null!;
    
    [Display(Name = "Zip File")]  
    public IFormFile ZipFile { set; get; } = null!;
}