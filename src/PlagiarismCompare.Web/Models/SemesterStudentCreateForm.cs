using System.ComponentModel.DataAnnotations;

namespace PlagiarismCompare.Web.Models;

public class SemesterStudentCreateForm
{
    [Display(Name = "Student")] 
    public int StudentId { get; set; }
    [Display(Name = "Semester")] 
    public int SemesterId { get; set; }
}