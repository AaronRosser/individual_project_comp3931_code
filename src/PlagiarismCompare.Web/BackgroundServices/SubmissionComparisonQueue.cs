using System.Collections.Concurrent;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Web.BackgroundServices;

// From https://blog.elmah.io/async-processing-of-long-running-tasks-in-asp-net-core/
public class SubmissionComparisonQueue
{
    private readonly ConcurrentQueue<Submission> _submissions;
    private readonly SemaphoreSlim _signal;

    public SubmissionComparisonQueue()
    {
        _submissions = new ConcurrentQueue<Submission>();
        _signal = new SemaphoreSlim(0);
    }
    
    public void Enqueue(Submission submission)
    {
        if (submission == null)
        {
            throw new ArgumentNullException(nameof(submission));
        }

        _submissions.Enqueue(submission);
        _signal.Release();
    }
    
    public async Task<Submission> DequeueAsync(CancellationToken cancellationToken)
    {
        await _signal.WaitAsync(cancellationToken);
        _submissions.TryDequeue(out var submission);

        return submission!;
    }
}