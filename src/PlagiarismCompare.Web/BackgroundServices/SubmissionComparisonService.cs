using PlagiarismCompare.Data;
using PlagiarismCompare.Services;

namespace PlagiarismCompare.Web.BackgroundServices;


// From https://blog.elmah.io/async-processing-of-long-running-tasks-in-asp-net-core/
public class SubmissionComparisonService : BackgroundService
{
    private readonly IServiceProvider _serviceProvider;
    private readonly SubmissionComparisonQueue _queue;

    public SubmissionComparisonService(IServiceProvider serviceProvider, SubmissionComparisonQueue queue)
    {
        _serviceProvider = serviceProvider;
        _queue = queue;
    }

    protected override async Task ExecuteAsync(CancellationToken cancellationToken)
    {
        while (!cancellationToken.IsCancellationRequested)
        {
            var submission = await _queue.DequeueAsync(cancellationToken);

            using IServiceScope scope = _serviceProvider.CreateScope();
            SubmissionComparisonHandler comparisonHandler = scope.ServiceProvider.GetRequiredService<SubmissionComparisonHandler>();

            await comparisonHandler.RunComparisons(submission);
        }
    }
}