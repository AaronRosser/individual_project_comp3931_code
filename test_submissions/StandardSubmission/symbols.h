#ifndef SYMBOLS_H
#define SYMBOLS_H

// #include "parser.h"

typedef int bool;
#define FALSE 0
#define TRUE 1

typedef struct str_array_struct {
	char arr[15][20];
	int arr_len;
} str_array;

bool is_string_in_array(const char *str, const str_array *arr);
void panic(char *error_msg);

// Symbol table

#define MAX_NUM_CHILDREN 128

typedef enum {
    undefined,

    class,

    constructor,
    method,
    function,

    static_var,
    field,
    argument,
    local_var
} Symbol_Type;

typedef struct Symbol_S {
    char name[128];
    char type[128];

    Symbol_Type symbol_type;
    bool is_initialized;

    int address;
    int num_statics;
    int num_fields;
    int num_args;
    int num_locals;

    struct Symbol_S *parent;
    struct Symbol_S *child_symbols[MAX_NUM_CHILDREN];
    int num_children;
} Symbol;

typedef enum {
    no_err = 0,
	undecId = 16,		// undeclared identifier (e.g. class, subroutine, or variable)
	redecId	= 17	// redeclaration of identifier in the same scope
} SymbolErrors;

SymbolErrors add_symbol(const char* name, const char* type, Symbol_Type symbol_type);
Symbol_Type get_symbol_type(const char *symbol_type);
Symbol *find_symbol(const char* name);
Symbol *find_class_symbol(const char *class_name, const char* name);
void init_table();
void free_table();
void set_second_round();
bool is_second_round();

#endif
