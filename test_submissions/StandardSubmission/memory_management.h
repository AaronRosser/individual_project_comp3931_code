#include "kernel/types.h"
#include "user/user.h"

void _free(void *ap);

void* _malloc(uint nbytes);