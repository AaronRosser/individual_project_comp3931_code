/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Lexer Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:			Aaron Rosser
Student ID:				201319759
Email:					sc19ar@leeds.ac.uk
Date Work Commenced:	19/02/21
*************************************************************************/
#define TEST

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "lexer.h"

typedef int bool;
#define FALSE 0
#define TRUE 1

#define IS_DIGIT(x) ((x) >= '0' && (x) <= '9')
#define IS_LOWERCASE(x) ((x) >= 'a' && (x) <= 'z')
#define IS_UPPERCASE(x) ((x) >= 'A' && (x) <= 'Z')
#define IS_ALPHABET(x) ((IS_LOWERCASE(x)) || IS_UPPERCASE(x))
#define IS_IDENTIFIER_CHAR(x) ((IS_ALPHABET(x)) || (IS_DIGIT(x)) || (x) == '_')

#define NUM_SYMBOLS 19
static char symbols[] = {
	'(', ')', '[', ']', '{', '}', ',', ';', '=', '.', '+', '-', '*', '/', '&', '|', '~', '<', '>'
};

#define NUM_RES_WORDS 21
static char* res_words[] = {
	"boolean", "char", "class", "constructor", "do", "else", "false",
	"field", "function", "if", "int", "let", "method", "null", "return",
	"static", "this", "true", "var", "void", "while"
};

// Text of current jack file
static char *f_text_root;
static char *f_text;

static char *f_name;
static int f_line_number;

void set_error(Token *t, LexErrCodes code, char *msg) {
	t->tp = ERR;
	t->ec = code;
	// snprintf(t->lx, sizeof(t->lx), "Error, line: %d, near [%s], reason: %s", f_line_number, "Error here", msg);
	snprintf(t->lx, sizeof(t->lx), "Error: %s", msg);
}

/**
 * Copies symbol to token if found and increments pointer by 1
 * @return TRUE on find, FALSE otherwise
 */
int get_symbol(Token *t) {
	for (int i = 0; i < NUM_SYMBOLS; ++i) {
		if (f_text[0] == symbols[i]) {
			snprintf(t->lx, 2, "%s", f_text);
			t->tp = SYMBOL;
			f_text += 1;
			return TRUE;
		}
	}
	// Not valid symbol
	return FALSE;
}

/**
 * Copies reserved word to token if found and increments pointer by word length
 * @return TRUE on find, FALSE otherwise
 */
int get_reserved_word(Token *t) {
	for (int i = 0; i < NUM_RES_WORDS; ++i) {
		int len = strlen(res_words[i]);
		// Check if is reserved word and the next character would not make it an identifier
		if (strncmp(f_text, res_words[i], len) == 0 && !IS_IDENTIFIER_CHAR(f_text[len])) {
			snprintf(t->lx, len + 1, "%s", f_text);
			t->tp = RESWORD;
			f_text += len;
			return TRUE;
		}
	}
	// Not reserved word
	return FALSE;
}

/**
 * Copies digits to token and increments pointer by number of digits
 * @return TRUE on find or error, FALSE otherwise
 */
bool get_integer(Token *t) {
	if (!IS_DIGIT(f_text[0])) {
		// Not int
		return FALSE;
	}

	int i = 0;
	while(IS_DIGIT(f_text[i])) {
		++i;
	}

	//! Illegal character part of integer

	snprintf(t->lx, i + 1, "%s", f_text);
	t->tp = INT;
	f_text += i;
	return TRUE;
}

/**
 * Copies string to token if found and increments pointer by string length
 * @return TRUE on find or error, FALSE otherwise
 */
bool get_string(Token *t) {
	if (f_text[0] != '"') {
		// Not string
		return FALSE;
	}

	int i = 1;
	while(f_text[i] != '"' && f_text[i] != '\n' && f_text[i] != '\0') {
		++i;
	}

	if (f_text[i] == '\n') {
		set_error(t, NewLnInStr, "new line in string constant");
		return TRUE;
	} else if (f_text[i] == '\0') {
		set_error(t, EofInStr, "unexpected eof in string constant");
		return TRUE;
	}

	snprintf(t->lx, i, "%s", f_text + 1);
	t->tp = STRING;
	f_text += i + 1;
	return TRUE;
}

/**
 * Copies identifier to token if found and increments pointer by identifier length
 * @return TRUE on find or error, FALSE otherwise
 */
bool get_identifier(Token *t) {
	if (!IS_ALPHABET(f_text[0]) && f_text[0] != '_') {
		// Not valid identifier
		return FALSE;
	}

	int i = 1;
	while(IS_IDENTIFIER_CHAR(f_text[i])) {
		++i;
	}

	//! Illegal character part of identifier

	snprintf(t->lx, i + 1, "%s", f_text);
	t->tp = ID;
	f_text += i;
	return TRUE;
}

/**
 * Sets lexeme to "End of File" if next character is null terminator
 * @return TRUE on find, FALSE otherwise
 */
bool get_EOFile(Token *t) {
	if (f_text[0] != '\0') {
		return FALSE;
	}

	snprintf(t->lx, 128, "%s", "End of File");
	t->tp = EOFile;
	// Don't increment past \0
	return TRUE;
}

/**
 * Increments pointer while next character is whitespace, a tab, carriage return or newline and
 * increments line number with every newline
 */
void _consume_ws_new_lines() {
	while (f_text[0] == ' ' || f_text[0] == '\t' || f_text[0] == '\r' || f_text[0] == '\n') {
		if (f_text[0] == '\n') {
			++f_line_number;
		}
		++f_text;
	}
}

/**
 * If the next two characters are // increments pointer until next newline or null terminator
 */
void _consume_simple_comment() {
	if (f_text[0] != '/' || f_text[1] != '/') {
		// Not comment
		return;
	}
	f_text += 2;

	// Consume to end of line
	while (f_text[0] != '\0' && f_text[0] != '\n') {
		++f_text;
	}
}

/**
 * If next two characters are /* increments pointer until next * / or null terminator and increments
 * line number with every newline
 * @return TRUE on error
 */
bool _consume_multiline_comment(Token *t) {
	if (f_text[0] != '/' || f_text[1] != '*') {
		// Not multiline comment
		return FALSE;
	}
	f_text += 2;

	// Consume until next */
	while (f_text[0] != '\0' && !(f_text[0] == '*' && f_text[1] == '/')) {
		if (f_text[0] == '\n') {
			++f_line_number;
		}
		++f_text;
	}
	
	if (f_text[0] == '\0') {
		set_error(t, EofInCom, "unexpected eof in comment");
		return TRUE;
	}

	f_text += 2;
	return FALSE;
}

/**
 * Increments pointer until there is no more comments or whitespace or an error is encountered
 * @return TRUE on error
 */
bool consume_comments_cw_new_lines(Token *t) {
	// Remove leading whitespace / new lines
	_consume_ws_new_lines();

	// Keep removing comments and trailing whitespace until no more
	char *prev_f_text = NULL;
	bool error = FALSE;
	while (prev_f_text != f_text && error == FALSE) {
		prev_f_text = f_text;
		_consume_simple_comment();
		error = _consume_multiline_comment(t);

		// Remove trailing whitespace / new lines
		_consume_ws_new_lines();
	}

	return error;
}

/**
 * Reads file until end of next token or error
 * @return next token
 */
Token get_next_token(bool peek) {
	char *temp_offset = f_text;
	int temp_line_number = f_line_number;

	Token t;
	// Set filename
	snprintf(t.fl, sizeof(t.fl), "%s", f_name);	

	// Try and get lexeme and type
	if (consume_comments_cw_new_lines(&t)) ;// Breaks on error
	else if (get_symbol(&t)) ;				// Breaks on error or success
	else if (get_reserved_word(&t)) ;
	else if (get_integer(&t)) ;
	else if (get_string(&t)) ;
	else if (get_identifier(&t)) ;
	else if (get_EOFile(&t)) ;
	else {
		set_error(&t, IllSym, "illegal symbol in source file");
	}

	// Set line number
	t.ln = f_line_number;

	if (peek) {
		// Revert globals
		f_line_number = temp_line_number;
		f_text = temp_offset;
	}

	return t;
}

/**
 * Reads file to buffer
 * @return 1 on success, 0 on fail
 */
int InitLexer(char* file_name)
{
	f_name = malloc(sizeof(char[strlen(file_name) + 1]));
	snprintf(f_name, sizeof(char[strlen(file_name) + 1]), "%s", file_name);

	FILE *f = fopen(file_name, "rb");
	if (f == NULL) {
		return 0;
	}

	// Get file size and allocate memory
	fseek(f, 0, SEEK_END);
	long file_size = ftell(f);
	f_text_root = malloc(file_size + 1);

	// Read file to buffer
	fseek(f, 0, SEEK_SET);
	fread(f_text_root, 1, file_size, f);
	fclose(f);

	// Null terminate
	f_text_root[file_size] = 0;

	f_text = f_text_root;
	f_line_number = 1;

	return 1;
}

Token GetNextToken()
{
	return get_next_token(FALSE);
}

Token PeekNextToken()
{
	return get_next_token(TRUE);
}

/**
 * Frees dynamically allocated memory
 */
int StopLexer()
{
	free(f_text_root);
	free(f_name);
	return 0;
}
