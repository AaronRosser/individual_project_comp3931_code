/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Parser Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:			Aaron Rosser
Student ID:				201319759
Email:					sc19ar@leeds.ac.uk
Date Work Commenced:	26/04/21
*************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

//#include "lexer.h"
#include "parser.h"
#include "symbols.h"
//#include "compiler.h"
void write_command(const char *command);

typedef int bool;
#define FALSE 0
#define TRUE 1

static char filename[32];
static int if_count = 0;
static int while_count = 0;

// String arrays for grammars with lists of strings
const str_array operand_lexemes 		= { .arr_len = 4, .arr = {"true", "false", "null", "this"} };
const str_array factor_lexemes 			= { .arr_len = 2, .arr = {"-", "~"} };
const str_array term_lexemes 			= { .arr_len = 2, .arr = {"*", "/"} };
const str_array arithmetic_exp_lexemes 	= { .arr_len = 2, .arr = {"+", "-"} };
const str_array relational_exp_lexemes 	= { .arr_len = 3, .arr = {"=", ">", "<"} };
const str_array exp_lexemes 			= { .arr_len = 2, .arr = {"&", "|"} };
const str_array statement_lexemes 		= { .arr_len = 6, .arr = {"var", "let", "if", "while", "do", "return"} };
const str_array subroutine_lexemes 		= { .arr_len = 3, .arr = {"constructor", "function", "method"} };
const str_array type_lexemes 			= { .arr_len = 3, .arr = {"int", "char", "boolean"} };
const str_array class_var_lexemes 		= { .arr_len = 2, .arr = {"static", "field"} };
const str_array class_declar_lexemes 	= { .arr_len = 5, .arr = {"static", "field", "constructor", "function", "method"} };

// Forward declaration of functions
ParserInfo type();
ParserInfo statement();
ParserInfo expression();
ParserInfo expressionList();

// Helper functions
bool is_next_token_lexeme_in_array(const str_array *arr) {
	Token t = PeekNextToken();
	return is_string_in_array(t.lx, arr);
}

bool is_token_lexeme_char(const Token *t, char c) {
	return t->lx[0] == c && t->lx[1] == '\0';
}

bool is_next_token_lexeme_char(char c) {
	Token t = PeekNextToken();
	return t.lx[0] == c && t.lx[1] == '\0';
}

ParserInfo success() {
	ParserInfo pi;
	pi.er = none;
	return pi;
}

ParserInfo error(SyntaxErrors error_code, Token token) {
	ParserInfo pi;
	pi.tk = token;

	if (token.tp == ERR)
		pi.er = lexerErr;
	else
		pi.er = error_code;
	
	return pi;
}

void push_identifier(Symbol *symbol, bool that) {
	char command[256];
	if (symbol->symbol_type == static_var)
		sprintf(command, "push static %i", symbol->address);
	else if (symbol->symbol_type == field && that)
		sprintf(command, "push that %i", symbol->address);
	else if (symbol->symbol_type == field && !that)
		sprintf(command, "push this %i", symbol->address);
	else if (symbol->symbol_type == argument)
		sprintf(command, "push argument %i", symbol->address);
	else if (symbol->symbol_type == local_var)
		sprintf(command, "push local %i", symbol->address);
	else
		return;
	
	write_command(command);
}

void pop_identifier(Symbol *symbol, bool that) {
	char command[256];
	if (symbol->symbol_type == static_var)
		sprintf(command, "pop static %i", symbol->address);
	else if (symbol->symbol_type == field && that)
		sprintf(command, "pop that %i", symbol->address);
	else if (symbol->symbol_type == field && !that)
		sprintf(command, "pop this %i", symbol->address);
	else if (symbol->symbol_type == argument)
		sprintf(command, "pop argument %i", symbol->address);
	else if (symbol->symbol_type == local_var)
		sprintf(command, "pop local %i", symbol->address);
	else
		return;
	
	write_command(command);
}

bool is_expression_next() {
	// expression -> relationalExpression { ( & | | ) relationalExpression }
	// relationalExpression -> arithmeticExpression { ( = | > | < ) arithmeticExpression }
	// arithmeticExpression -> term { ( + | - ) term }
	// term -> factor { ( * | / ) factor }
	// factor -> ( - | ~ | ε ) operand

	//! - | ~
	Token next_token = PeekNextToken();
	if (is_string_in_array(next_token.lx, &factor_lexemes))
		return TRUE;

	// operand -> integerConstant | identifier [.identifier ] [ [ expression ] | (expressionList ) ] | (expression) | stringLiteral | true | false | null | this
	//! true | false | null | this
	if (is_string_in_array(next_token.lx, &operand_lexemes))
		return TRUE;

	//! integerConstant | identifier | stringLiteral
	if (next_token.tp == INT || next_token.tp == ID || next_token.tp == STRING)
		return TRUE;

	if (is_token_lexeme_char(&next_token, '(')) {
		// Can't recursively check for (((((expression))))) because peek doesn't consume ((
		return TRUE;
	}

	return FALSE;
}

// identifier [.identifier ] [ [ expression ] | (expressionList ) ]
ParserInfo identifier_expression_list() {
	// ClassName.static_var
	// ObjectName.static_var

	// ClassName.function()
	// ObjectName.function()

	// ObjectName.method()
	// ObjectName.variable

	// method()
	// array[]
	// variable

	char command_buffer[512];
	char class_obj_name[128];
	char var_method_name[128];

	//! identifier
	Token next_token = GetNextToken();
	if (next_token.tp != ID)
		return error(idExpected, next_token);

	strcpy(class_obj_name, next_token.lx);

	//! [.identifier ]
	if (is_next_token_lexeme_char('.')) {
		// Consume .
		GetNextToken();

		next_token = GetNextToken();
		if (next_token.tp != ID)
			return error(idExpected, next_token);

		strcpy(var_method_name, next_token.lx);
	} else {
		strcpy(var_method_name, class_obj_name);
		strcpy(class_obj_name, "this");
	}

	Symbol *object = find_symbol(class_obj_name);
	if (is_second_round()) {
		if (object == NULL)
			return error(undecIdentifier, next_token);

		if (object->symbol_type != class)
			push_identifier(object, FALSE);
	}


	//! [ [ expression ] | (expressionList ) ]
	if (is_next_token_lexeme_char('[')) {
		// Array
		// Consume [
		GetNextToken();

		//! expression
		ParserInfo pi = expression();
		if (pi.er != none)
			return pi;

		if (strcmp(class_obj_name, "this") != 0) {
			write_command("pop this 1");
		}

		if (is_second_round()) {
			Symbol *var_symbol = find_class_symbol(object->type, var_method_name);
			if (var_symbol == NULL || (var_symbol->symbol_type != field && var_symbol->symbol_type != static_var && var_symbol->symbol_type != argument && var_symbol->symbol_type != local_var))
				return error(undecIdentifier, next_token);
			
			push_identifier(var_symbol, strcmp(class_obj_name, "this") != 0);
		}

		write_command("add");
		write_command("pop pointer 1");
		write_command("push that 0");

		//! ]
		next_token = GetNextToken();
		if (!is_token_lexeme_char(&next_token, ']'))
			return error(closeBracketExpected, next_token);

	} else if (is_next_token_lexeme_char('(')) {
		// Function / method
		// Consume (
		GetNextToken();

		//! expressionList
		ParserInfo pi = expressionList();
		if (pi.er != none)
			return pi;

		if (is_second_round()) {
			Symbol *subroutine_symbol = find_class_symbol(object->type, var_method_name);
			if (subroutine_symbol == NULL || (subroutine_symbol->symbol_type != constructor && subroutine_symbol->symbol_type != method && subroutine_symbol->symbol_type != function))
				return error(undecIdentifier, next_token);

			sprintf(command_buffer, "call %s.%s %i", object->type, subroutine_symbol->name, subroutine_symbol->num_args);
			write_command(command_buffer);
		}

		//! )
		next_token = GetNextToken();
		if (!is_token_lexeme_char(&next_token, ')'))
			return error(closeParenExpected, next_token);
	} else {
		// Variable
		if (strcmp(class_obj_name, "this") != 0) {
			write_command("pop this 1");
		}

		if (is_second_round()) {
			Symbol *var_symbol = find_class_symbol(object->type, var_method_name);
			if (var_symbol == NULL || (var_symbol->symbol_type != field && var_symbol->symbol_type != static_var && var_symbol->symbol_type != argument && var_symbol->symbol_type != local_var))
				return error(undecIdentifier, next_token);
			
			push_identifier(var_symbol, strcmp(class_obj_name, "this") != 0);
		}
	}

	return success();
}

// operand -> integerConstant | identifier [.identifier ] [ [ expression ] | (expressionList ) ] | (expression) | stringLiteral | true | false | null | this
ParserInfo operand() {
	char command[512];
	Token next_token = PeekNextToken();

	//! integerConstant
	if (next_token.tp == INT) {
		//  Consume int
		GetNextToken();

		sprintf(command, "push constant %s", next_token.lx);
		write_command(command);
		return success();
	}

	//! identifier [.identifier ] [ [ expression ] | (expressionList ) ]
	if (next_token.tp == ID)
		return identifier_expression_list();

	//! (expression)
	if (is_token_lexeme_char(&next_token, '(')) {
		// Consume (
		GetNextToken();

		ParserInfo pi = expression();
		if (pi.er != none)
			return pi;

		//! )
		next_token = GetNextToken();
		if (!is_token_lexeme_char(&next_token, ')'))
			return error(closeParenExpected, next_token);

		return success();
	}

	if (next_token.tp == STRING) {
		// Consume string
		GetNextToken();

		sprintf(command, "push constant %i", (int)strlen(next_token.lx));
		write_command(command);

		sprintf(command, "call String.new 1");
		write_command(command);

		for (int i = 0; i < strlen(next_token.lx); ++i) {
			sprintf(command, "push constant %i", next_token.lx[i]);
			write_command(command);

			strcpy(command, "call String.appendChar 2");
			write_command(command);
		}
		return success();
	}

	//! true | false | null | this
	if (strcmp(next_token.lx, "true") == 0) {
		GetNextToken();
		write_command("push constant 0");
		write_command("not");
		return success();
	} else if (strcmp(next_token.lx, "false") == 0) {
		GetNextToken();
		write_command("push constant 0");
		return success();
	} else if (strcmp(next_token.lx, "null") == 0) {
		GetNextToken();
		write_command("push constant 0");
		return success();
	}  else if (strcmp(next_token.lx, "this") == 0) {
		GetNextToken();
		write_command("push pointer 0");
		return success();
	}

	return error(syntaxError, next_token);
}

// factor -> ( - | ~ | ε ) operand
ParserInfo factor() {
	//! - | ~ | ε
	Token next_token = PeekNextToken();
	if (is_string_in_array(next_token.lx, &factor_lexemes)) {
		// Consume - | ~
		GetNextToken();
	}

	//! operand
	ParserInfo pi = operand();
	if (pi.er != none)
		return pi;

	if (strcmp(next_token.lx, "-") == 0)
			write_command("neg");
	else if (strcmp(next_token.lx, "~") == 0)
		write_command("not");

	return success();
}

// term -> factor { ( * | / ) factor }
ParserInfo term() {
	//! factor
	ParserInfo pi = factor();
	if (pi.er != none)
		return pi;

	//! { ( * | / ) factor }
	Token next_token = PeekNextToken();
	while (is_string_in_array(next_token.lx, &term_lexemes)) {
		// Consume * | /
		GetNextToken();

		//! factor
		pi = factor();
		if (pi.er != none)
			return pi;

		if (strcmp(next_token.lx, "*") == 0)
			write_command("call Math.multiply 2");
		else if (strcmp(next_token.lx, "/") == 0)
			write_command("call Math.divide 2");

		next_token = PeekNextToken();
	}

	return success();
}

// arithmeticExpression -> term { ( + | - ) term }
ParserInfo arithmeticExpression() {
	//! term
	ParserInfo pi = term();
	if (pi.er != none)
		return pi;

	//! { ( + | - ) term }
	Token next_token = PeekNextToken();
	while (is_string_in_array(next_token.lx, &arithmetic_exp_lexemes)) {
		// Consume + | -
		GetNextToken();

		//! term
		pi = term();
		if (pi.er != none)
			return pi;

		if (strcmp(next_token.lx, "-") == 0)
			write_command("sub");
		else
			write_command("add");

		next_token = PeekNextToken();
	}

	return success();
}

// relationalExpression -> arithmeticExpression { ( = | > | < ) arithmeticExpression }
ParserInfo relationalExpression() {
	//! arithmeticExpression
	ParserInfo pi = arithmeticExpression();
	if (pi.er != none)
		return pi;

	//! { ( = | > | < ) arithmeticExpression }
	Token next_token = PeekNextToken();
	while (is_string_in_array(next_token.lx, &relational_exp_lexemes)) {
		// Consume = | > | < 
		GetNextToken();

		//! arithmeticExpression
		pi = arithmeticExpression();
		if (pi.er != none)
			return pi;

		if (strcmp(next_token.lx, "=") == 0)
			write_command("eq");
		else if (strcmp(next_token.lx, "<") == 0)
			write_command("lt");
		else
			write_command("gt");

		next_token = PeekNextToken();
	}

	return success();
}

// expression -> relationalExpression { ( & | | ) relationalExpression }
ParserInfo expression() {
	//! relationalExpression
	ParserInfo pi = relationalExpression();
	if (pi.er != none)
		return pi;

	//! { ( & | | ) relationalExpression }
	Token next_token = PeekNextToken();
	while (is_string_in_array(next_token.lx, &exp_lexemes)) {
		// Consume & | |
		GetNextToken();

		//! relationalExpression
		pi = relationalExpression();
		if (pi.er != none)
			return pi;

		if (strcmp(next_token.lx, "&") == 0)
			write_command("and");
		else
			write_command("or");

		next_token = PeekNextToken();
	}

	return success();
}

// returnStatemnt -> return [ expression ] ;
ParserInfo returnStatemnt() {
	Token next_token = GetNextToken();
	//! return
	if (strcmp(next_token.lx, "return") != 0)
		return error(syntaxError, next_token);

	if (is_expression_next()) {
		//! expression
		ParserInfo pi = expression();
		if (pi.er != none)
			return pi;
	} else {
		write_command("push constant 0");
	}

	write_command("return");

	//! ;
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ';'))
		return error(semicolonExpected, next_token);

	return success();
}

// expressionList -> expression { , expression } | ε
ParserInfo expressionList() {
	//! ε
	if (!is_expression_next())
		return success();

	//! expression
	ParserInfo pi = expression();
	if (pi.er != none)
		return pi;

	//! { , expression }
	while (is_next_token_lexeme_char(',')) {
		// Consume ,
		GetNextToken();

		pi = expression();
		if (pi.er != none)
			return pi;
	}

	return success();
}

// subroutineCall -> identifier [ . identifier ] ( expressionList )
ParserInfo subroutineCall() {
	char command_buffer[512];
	char class_obj_name[128];
	char var_method_name[128];

	//! identifier
	Token class_var_token = GetNextToken();
	if (class_var_token.tp != ID)
		return error(idExpected, class_var_token);

	strcpy(class_obj_name, class_var_token.lx);

	//! [ . identifier ]
	Token next_token = class_var_token;
	if (is_next_token_lexeme_char('.')) {
		// Consume .
		GetNextToken();

		next_token = GetNextToken();
		if (next_token.tp != ID)
			return error(idExpected, next_token);

		strcpy(var_method_name, next_token.lx);
	} else {
		strcpy(var_method_name, class_obj_name);
		strcpy(class_obj_name, "this");
	}

	Symbol *object = find_symbol(class_obj_name);
	Symbol *subroutine_symbol;
	if (is_second_round()) {
		if (object == NULL)
			return error(undecIdentifier, class_var_token);

		if (object->symbol_type != class)
			push_identifier(object, FALSE);

		subroutine_symbol = find_class_symbol(object->type, var_method_name);
		if (subroutine_symbol == NULL || (subroutine_symbol->symbol_type != constructor && subroutine_symbol->symbol_type != method && subroutine_symbol->symbol_type != function))
			return error(undecIdentifier, next_token);

		if (strcmp(class_obj_name, "this") == 0 && subroutine_symbol->symbol_type == method)
			write_command("push pointer 0");
	}

	//! (
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '('))
		return error(openParenExpected, next_token);

	//! expressionList
	ParserInfo pi = expressionList();
	if (pi.er != none)
		return pi;

	if (is_second_round()) {
		sprintf(command_buffer, "call %s.%s %i", object->type, subroutine_symbol->name, subroutine_symbol->num_args);
		write_command(command_buffer);
		write_command("pop temp 0");
	}

	//! )
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ')'))
		return error(closeParenExpected, next_token);

	return success();
}

// doStatement -> do subroutineCall ;
ParserInfo doStatement() {
	//! do
	Token next_token = GetNextToken();
	if (strcmp(next_token.lx, "do") != 0)
		return error(syntaxError, next_token);

	//! subroutineCall
	ParserInfo pi = subroutineCall();
	if (pi.er != none)
		return pi;

	//! ;
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ';'))
		return error(semicolonExpected, next_token);

	return success();
}

// whileStatement -> while ( expression ) { {statement} }
ParserInfo whileStatement() {
	int current_while_count = while_count++;
	char command[128];

	sprintf(command, "label WHILE_EXP%i", current_while_count);
	write_command(command);

	//! while
	Token next_token = GetNextToken();
	if (strcmp(next_token.lx, "while") != 0)
		return error(syntaxError, next_token);

	//! (
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '('))
		return error(openParenExpected, next_token);

	//! expression
	ParserInfo pi = expression();
	if (pi.er != none)
		return pi;

	//! )
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ')'))
		return error(closeParenExpected, next_token);

	write_command("not");

	sprintf(command, "if-goto WHILE_END%i", current_while_count);
	write_command(command);

	//! {
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '{'))
		return error(openBraceExpected, next_token);

	//! {statement}
	while (is_next_token_lexeme_in_array(&statement_lexemes)) {
		pi = statement();
		if (pi.er != none)
			return pi;
	}

	//! }
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '}'))
		return error(closeBraceExpected, next_token);

	sprintf(command, "goto WHILE_EXP%i", current_while_count);
	write_command(command);

	sprintf(command, "label WHILE_END%i", current_while_count);
	write_command(command);

	return success();
}

// ifStatement -> if ( expression ) { {statement} } [else { {statement} }]
ParserInfo ifStatement() {
	char command[128];
	int current_if_count = if_count++;
	ParserInfo pi;

	//! if
	Token next_token = GetNextToken();
	if (strcmp(next_token.lx, "if") != 0)
		return error(syntaxError, next_token);

	//! (
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '('))
		return error(openParenExpected, next_token);

	//! expression
	pi = expression();
	if (pi.er != none)
		return pi;

	//! )
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ')'))
		return error(closeParenExpected, next_token);

	sprintf(command, "if-goto IF_TRUE%i", current_if_count);
	write_command(command);

	sprintf(command, "goto IF_FALSE%i", current_if_count);
	write_command(command);

	sprintf(command, "label IF_TRUE%i", current_if_count);
	write_command(command);

	//! {
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '{'))
		return error(openBraceExpected, next_token);

	//! {statement}
	while (is_next_token_lexeme_in_array(&statement_lexemes)) {
		pi = statement();
		if (pi.er != none)
			return pi;
	}

	//! }
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '}'))
		return error(closeBraceExpected, next_token);

	//! [else { {statement} }]
	next_token = PeekNextToken();
	if (strcmp(next_token.lx, "else") == 0) {
		// Consume else
		GetNextToken();

		sprintf(command, "goto IF_END%i", current_if_count);
		write_command(command);

		sprintf(command, "label IF_FALSE%i", current_if_count);
		write_command(command);

		//! {
		next_token = GetNextToken();
		if (!is_token_lexeme_char(&next_token, '{'))
			return error(openBraceExpected, next_token);

		//! {statement}
		while (is_next_token_lexeme_in_array(&statement_lexemes)) {
			pi = statement();
			if (pi.er != none)
				return pi;
		}

		//! }
		next_token = GetNextToken();
		if (!is_token_lexeme_char(&next_token, '}'))
			return error(closeBraceExpected, next_token);

		sprintf(command, "label IF_END%i", current_if_count);
		write_command(command);
	} else {
		sprintf(command, "label IF_FALSE%i", current_if_count);
		write_command(command);
	}

	return success();
}

// letStatemnt -> let identifier [ [ expression ] ] = expression ;
ParserInfo letStatemnt() {
	bool array = FALSE;
	ParserInfo pi;

	//! let
	Token next_token = GetNextToken();
	if (strcmp(next_token.lx, "let") != 0)
		return error(syntaxError, next_token);

	//! identifier
	next_token = GetNextToken();
	if (next_token.tp != ID)
		return error(idExpected, next_token);

	Symbol *var_symbol = find_symbol(next_token.lx);
	if (is_second_round()) {
		if (var_symbol == NULL || (var_symbol->symbol_type != field && var_symbol->symbol_type != static_var && var_symbol->symbol_type != argument && var_symbol->symbol_type != local_var))
			return error(undecIdentifier, next_token);
	}

	//! [ [ expression ] ]
	if (is_next_token_lexeme_char('[')) {
		array = TRUE;

		// Consume [
		GetNextToken();

		pi = expression();
		if (pi.er != none)
			return pi;

		next_token = GetNextToken();
		if (!is_token_lexeme_char(&next_token, ']'))
			return error(closeBracketExpected, next_token);

		if (is_second_round()) {
			push_identifier(var_symbol, FALSE);
			write_command("add");
		}
	}

	//! =
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '='))
		return error(equalExpected, next_token);

	//! expression
	pi = expression();
	if (pi.er != none)
		return pi;

	
	if (array) {	
		write_command("pop temp 0");
		write_command("pop pointer 1");
		write_command("push temp 0");
		write_command("pop that 0");
	} else {
		if (is_second_round()) {
			pop_identifier(var_symbol, FALSE);
		}
	}

	//! ;
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ';'))
		return error(semicolonExpected, next_token);

	return success();
}

// varDeclarStatement -> var type identifier { , identifier } ;
ParserInfo varDeclarStatement() {
	Token next_token = GetNextToken();
	if (strcmp(next_token.lx, "var") != 0)
		return error(syntaxError, next_token);

	//! type
	Token type_token = PeekNextToken();
	ParserInfo pi = type();
	if (pi.er != none)
		return pi;

	//! identifier
	next_token = GetNextToken();
	if (next_token.tp != ID)
		return error(idExpected, next_token);

	SymbolErrors err = add_symbol(next_token.lx, type_token.lx, local_var);
	if (err == undecId)
		return error(undecIdentifier, type_token);
	else if (err == redecId)
		return error(redecIdentifier, next_token);

	//! {, identifier}
	while (is_next_token_lexeme_char(',')) {
		// Consume ,
		GetNextToken();

		next_token = GetNextToken();
		if (next_token.tp != ID)
			return error(idExpected, next_token);

		err = add_symbol(next_token.lx, type_token.lx, local_var);
		if (err == undecId)
			return error(undecIdentifier, type_token);
		else if (err == redecId)
			return error(redecIdentifier, next_token);
	}
	
	//! ;
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ';'))
		return error(semicolonExpected, next_token);

	return success();
}

// statement -> varDeclarStatement | letStatemnt | ifStatement | whileStatement | doStatement | returnStatemnt
ParserInfo statement() {
	Token next_token = PeekNextToken();

	if (strcmp(next_token.lx, "var") == 0)
		return varDeclarStatement();

	if (strcmp(next_token.lx, "let") == 0)
		return letStatemnt();
	
	if (strcmp(next_token.lx, "if") == 0)
		return ifStatement();
	
	if (strcmp(next_token.lx, "while") == 0)
		return whileStatement();
	
	if (strcmp(next_token.lx, "do") == 0)
		return doStatement();
	
	if (strcmp(next_token.lx, "return") == 0)
		return returnStatemnt();
	
	return error(syntaxError, next_token);	
}

// subroutineBody -> { {statement} }
ParserInfo subroutineBody() {
	//! {
	Token next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '{'))
		return error(openBraceExpected, next_token);

	//! {statement}
	while (is_next_token_lexeme_in_array(&statement_lexemes)) {
		ParserInfo pi = statement();
		if (pi.er != none)
			return pi;
	}
	
	//! }
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '}'))
		return error(closeBraceExpected, next_token);

	return success();
}

// paramList -> type identifier {, type identifier} | ε
ParserInfo paramList() {
	//! ε
	Token next_token = PeekNextToken();
	if (!is_string_in_array(next_token.lx, &type_lexemes) && next_token.tp != ID)
		return success();
	
	//! type
	Token type_token = PeekNextToken();
	ParserInfo pi = type();
	if (pi.er != none)
		return pi;

	//! identifier
	next_token = GetNextToken();
	if (next_token.tp != ID)
		return error(idExpected, next_token);

	SymbolErrors err = add_symbol(next_token.lx, type_token.lx, argument);
	if (err == undecId)
		return error(undecIdentifier, type_token);
	else if (err == redecId)
		return error(redecIdentifier, next_token);

	//! {, type identifier}
	while (is_next_token_lexeme_char(',')) {
		// Consume ,
		GetNextToken();

		pi = type();
		if (pi.er != none)
			return pi;

		next_token = GetNextToken();
		if (next_token.tp != ID)
			return error(idExpected, next_token);

		err = add_symbol(next_token.lx, type_token.lx, argument);
		if (err == undecId)
			return error(undecIdentifier, type_token);
		else if (err == redecId)
			return error(redecIdentifier, next_token);
	}

	return success();
}

// subroutineDeclar -> (constructor | function | method) (type|void) identifier (paramList) subroutineBody
ParserInfo subroutineDeclar() {
	if_count = 0;
	while_count = 0;

	ParserInfo pi;

	//! (constructor | function | method)
	Token sub_type_token = GetNextToken();
	if (!is_string_in_array(sub_type_token.lx, &subroutine_lexemes))
		return error(subroutineDeclarErr, sub_type_token);

	//! (type|void)
	Token return_type_token = PeekNextToken();
	if (strcmp(return_type_token.lx, "void") == 0) {
		// Consume void
		GetNextToken();
	} else {
		// Must be type
		pi = type();
		if (pi.er != none)
			return pi;
	}

	//! identifier
	Token next_token = GetNextToken();
	if (next_token.tp != ID)
		return error(idExpected, next_token);

	SymbolErrors err = add_symbol(next_token.lx, return_type_token.lx, get_symbol_type(sub_type_token.lx));
	if (err == undecId)
		return error(undecIdentifier, return_type_token);
	else if (err == redecId)
		return error(redecIdentifier, next_token);

	if (is_second_round()) {
		Symbol *subroutine_symbol = find_symbol(next_token.lx);
		if (subroutine_symbol == NULL || (subroutine_symbol->symbol_type != constructor && subroutine_symbol->symbol_type != method && subroutine_symbol->symbol_type != function))
			return error(undecIdentifier, next_token);
		
		char command[512];
		sprintf(command, "function %s.%s %i", subroutine_symbol->parent->name, subroutine_symbol->name, subroutine_symbol->num_locals);
		write_command(command);

		if (subroutine_symbol->symbol_type == constructor) {
			sprintf(command, "push constant %i", subroutine_symbol->parent->num_fields);
			write_command(command);

			write_command("call Memory.alloc 1");
			write_command("pop pointer 0");
		} else if (subroutine_symbol->symbol_type == method) {
			write_command("push argument 0");
			write_command("pop pointer 0");
		}
	}

	//! (
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '('))
		return error(openParenExpected, next_token);

	//! paramList
	pi = paramList();
	if (pi.er != none)
		return pi;

	//! )
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ')'))
		return error(closeParenExpected, next_token);

	//! subroutineBody
	return subroutineBody();
}

// type -> int | char | boolean | identifier
ParserInfo type() {
	Token next_token = GetNextToken();

	if (is_string_in_array(next_token.lx, &type_lexemes) || next_token.tp == ID)
		return success();

	return error(illegalType, next_token);
}

// classVarDeclar -> (static | field) type identifier {, identifier} ;
ParserInfo classVarDeclar() {
	//! (static | field)
	Token symbol_type_token = GetNextToken();
	if (!is_string_in_array(symbol_type_token.lx, &class_var_lexemes))
		return error(classVarErr, symbol_type_token);

	//! type
	Token type_token = PeekNextToken();
	ParserInfo pi = type();
	if (pi.er != none)
		return pi;

	//! identifier
	Token next_token = GetNextToken();
	if (next_token.tp != ID)
		return error(idExpected, next_token);

	SymbolErrors err = add_symbol(next_token.lx, type_token.lx, get_symbol_type(symbol_type_token.lx));
	if (err == undecId)
		return error(undecIdentifier, type_token);
	else if (err == redecId)
		return error(redecIdentifier, next_token);

	//! {, identifier}
	while (is_next_token_lexeme_char(',')) {
		// Consume ,
		GetNextToken();

		next_token = GetNextToken();
		if (next_token.tp != ID)
			return error(idExpected, next_token);

		err = add_symbol(next_token.lx, type_token.lx, get_symbol_type(symbol_type_token.lx));
		if (err == undecId)
			return error(undecIdentifier, type_token);
		else if (err == redecId)
			return error(redecIdentifier, next_token);
	}

	//! ;
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, ';'))
		return error(semicolonExpected, next_token);

	return success();
}

// memberDeclar -> classVarDeclar | subroutineDeclar
ParserInfo memberDeclar() {
	Token next_token = PeekNextToken();

	//! classVarDeclar
	if (is_string_in_array(next_token.lx, &class_var_lexemes))
		return classVarDeclar();

	//! subroutineDeclar
	if (is_string_in_array(next_token.lx, &subroutine_lexemes))
		return subroutineDeclar();

	return error(memberDeclarErr, next_token);
}

// classDeclar -> class identifier { {memberDeclar} }
ParserInfo classDeclar() {
	//! class
	Token next_token = GetNextToken();
	if (strcmp(next_token.lx, "class") != 0)
		return error(classExpected, next_token);

	//! identifier
	next_token = GetNextToken();
	if (next_token.tp != ID)
		return error(idExpected, next_token);

	SymbolErrors err = add_symbol(next_token.lx, next_token.lx, class);
	if (err == undecId)
		return error(undecIdentifier, next_token);
	else if (err == redecId)
		return error(redecIdentifier, next_token);

	//! {
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '{'))
		return error(openBraceExpected, next_token);

	//! {memberDeclar}
	while (is_next_token_lexeme_in_array(&class_declar_lexemes)) {
		ParserInfo pi = memberDeclar();
		if (pi.er != none)
			return pi;
	}

	//! }
	next_token = GetNextToken();
	if (!is_token_lexeme_char(&next_token, '}'))
		return error(closeBraceExpected, next_token);

	return success();
}

int InitParser(char* file_name)
{
	strncpy(filename, file_name, 32);
	return InitLexer(file_name);
}

ParserInfo Parse()
{
	ParserInfo pi = classDeclar();
	return pi;
}

int StopParser()
{
	return StopLexer();
}
