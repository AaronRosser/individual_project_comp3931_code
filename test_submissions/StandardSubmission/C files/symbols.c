/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Symbol Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:			Aaron Rosser
Student ID:				201319759
Email:					sc19ar@leeds.ac.uk
Date Work Commenced:	27/04/21
*************************************************************************/

#include "symbols.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

bool is_string_in_array(const char *str, const str_array *arr) {
	for (int i = 0; i < arr->arr_len; i++) {
		if (strcmp(str, arr->arr[i]) == 0) {
			return TRUE;
		}
	}

	return FALSE;
}

void panic(char *error_msg) {
	printf("%s\n", error_msg);
	exit(1);
}

// Symbol table
static const str_array built_in_types = { 
    .arr_len = 11, 
    .arr = {"int", "char", "boolean"} 
};

bool first_round = TRUE;

static Symbol *symbol_table[MAX_NUM_CHILDREN];
static int num_classes = 0;

static Symbol *current_class = NULL;
static Symbol *current_subroutine = NULL;

bool is_declared(const char *name, Symbol *symbol) {
	if (!first_round)
		return FALSE;

	for (int i = 0; i < symbol->num_children; ++i) {
		if (strcmp(symbol->child_symbols[i]->name, name) == 0) {
			return TRUE;
		}
	}

	return FALSE;
}

static Symbol *find_symbol_from_arr(const char* name, Symbol *arr[], int arr_len) {
	for (int i = 0; i < arr_len; ++i) {
		if (strcmp(arr[i]->name, name) == 0)
			return arr[i];
	}

	return NULL;
}

SymbolErrors add_symbol(const char* name, const char* type, Symbol_Type symbol_type) {
	// On second round dont add only check type exists
	if (!first_round) {
		if (symbol_type == class) {
			current_class = find_symbol_from_arr(name, symbol_table, num_classes);
			if (current_class == NULL)
				return undecId;
			return no_err;
		}

		// Update current subroutine
		if (symbol_type == constructor || symbol_type == method || symbol_type == function) {
			current_subroutine = find_symbol_from_arr(name, current_class->child_symbols, current_class->num_children);
			if (current_subroutine == NULL)
				return undecId;
		}

		// Constructor must return class type
		if (symbol_type == constructor) {
			if (strcmp(type, current_class->name) == 0)
				return no_err;
			else {
				// return undecId;
			}
		}
		
		if (is_string_in_array(type, &built_in_types))
			return no_err;

		// Only method / function can have type void
		if ((symbol_type == method || symbol_type == function) && strcmp(type, "void") == 0)
			return no_err;

		// Check for class types
		for (int i = 0; i < num_classes; ++i) {
			if (strcmp(type, symbol_table[i]->type) == 0)
				return no_err;
		}
		
		return undecId;
	}
	
	Symbol *new_symbol = malloc(sizeof(Symbol));
	strcpy(new_symbol->name, name);
	strcpy(new_symbol->type, type);

	new_symbol->symbol_type = symbol_type;
	new_symbol->is_initialized = FALSE;

	new_symbol->address = -1;
	new_symbol->num_statics = 0;
	new_symbol->num_fields = 0;
	new_symbol->num_args = 0;
	new_symbol->num_locals = 0;

	if (symbol_type == method)
		new_symbol->num_args = 1;

	new_symbol->num_children = 0;

	if (symbol_type == class) {
		if (num_classes >= MAX_NUM_CHILDREN)
			panic("Error max number classes");

		// Check for redeclaration
		if (first_round) {
			for (int i = 0; i < num_classes; ++i) {
				if (strcmp(symbol_table[i]->name, name) == 0) {
					free(new_symbol);
					return redecId;
				}
			}
		}

		symbol_table[num_classes++] = new_symbol;
		current_class = new_symbol;
		current_subroutine = NULL;

		new_symbol->parent = NULL;
	} else if (symbol_type == constructor || symbol_type == method || symbol_type == function) {
		if (current_class == NULL)
			panic("Error: No current class");

		if (current_class->num_children >= MAX_NUM_CHILDREN)
			panic("Error: Max subroutines / class variables");

		// Check for redeclaration
		if (is_declared(name, current_class)) {
			free(new_symbol);
			return redecId;
		}

		current_class->child_symbols[current_class->num_children++] = new_symbol;
		current_subroutine = new_symbol;
		new_symbol->parent = current_class;
	} else if (symbol_type == static_var || symbol_type == field) {
		if (current_class == NULL)
			panic("Error: No current class");

		if (current_class->num_children >= MAX_NUM_CHILDREN)
			panic("Error: Max subroutines / class variables");

		// Check for redeclaration
		if (is_declared(name, current_class)) {
			free(new_symbol);
			return redecId;
		}

		current_class->child_symbols[current_class->num_children++] = new_symbol;
		new_symbol->parent = current_class;

		if (symbol_type == static_var)
			new_symbol->address = current_class->num_statics++;
		else
			new_symbol->address = current_class->num_fields++;
	} else if (symbol_type == argument || symbol_type == local_var) {
		if (current_subroutine == NULL)
			panic("Error: No current subroutine");

		if (current_subroutine->num_children >= MAX_NUM_CHILDREN)
			panic("Error max variables");

		// Check for redeclaration
		if (is_declared(name, current_subroutine)) {
			free(new_symbol);
			return redecId;
		}
		
		current_subroutine->child_symbols[current_subroutine->num_children++] = new_symbol;
		new_symbol->parent = current_subroutine;

		if (symbol_type == argument)
			new_symbol->address = current_subroutine->num_args++;
		else
			new_symbol->address = current_subroutine->num_locals++;
	}

	return no_err;
}

Symbol_Type get_symbol_type(const char *symbol_type) {
	if (strcmp(symbol_type, "class") == 0)
		return class;
	else if (strcmp(symbol_type, "constructor") == 0)
		return constructor;
	else if (strcmp(symbol_type, "method") == 0)
		return method;
	else if (strcmp(symbol_type, "function") == 0)
		return function;
	else if (strcmp(symbol_type, "static") == 0)
		return static_var;
	else if (strcmp(symbol_type, "field") == 0)
		return field;

	return undefined;
}

Symbol *find_symbol(const char* name) {
	if (strcmp(name, "this") == 0)
		return current_class;

	Symbol *symbol;;
	
	// Search local scope
	if (current_subroutine != NULL) {
		symbol = find_symbol_from_arr(name, current_subroutine->child_symbols, current_subroutine->num_children);
		if (symbol != NULL)
			return symbol;
	}

	// Search class scope
	if (current_class != NULL) {
		symbol = find_symbol_from_arr(name, current_class->child_symbols, current_class->num_children);
		if (symbol != NULL)
			return symbol;
	}

	// Search classes
	return find_symbol_from_arr(name, symbol_table, num_classes);
}

Symbol *find_class_symbol(const char *class_name, const char* name) {
	if (strcmp(class_name, "this") == 0 || strcmp(class_name, current_class->name) == 0)
		return find_symbol(name);
	
	for (int i = 0; i < num_classes; ++i) {
		if (strcmp(symbol_table[i]->name, class_name) != 0)
			continue;

		for (int j = 0; j < symbol_table[i]->num_children; ++j) {
			if (strcmp(symbol_table[i]->child_symbols[j]->name, name) == 0)
				return symbol_table[i]->child_symbols[j];
		}
	}

	return NULL;
}

void init_table() {
	free_table();
	first_round = TRUE;

	// Array
	add_symbol("Array", "Array", class);
	add_symbol("new", "Array", constructor);
	add_symbol("size", "int", argument);

	add_symbol("dispose", "void", method);

	// Keyboard
	add_symbol("Keyboard", "Keyboard", class);
	add_symbol("init", "void", function);
	add_symbol("keyPressed", "char", function);
	add_symbol("readChar", "char", function);

	add_symbol("readLine", "String", function);
	add_symbol("message", "String", argument);

	add_symbol("readInt", "int", function);
	add_symbol("message", "String", argument);

	// Math
	add_symbol("Math", "Math", class);
	add_symbol("init", "void", function);

	add_symbol("abs", "int", function);
	add_symbol("x", "int", argument);

	add_symbol("multiply", "int", function);
	add_symbol("x", "int", argument);
	add_symbol("y", "int", argument);

	add_symbol("divide", "int", function);
	add_symbol("x", "int", argument);
	add_symbol("y", "int", argument);

	add_symbol("sqrt", "int", function);
	add_symbol("x", "int", argument);

	add_symbol("max", "int", function);
	add_symbol("a", "int", argument);
	add_symbol("b", "int", argument);

	add_symbol("min", "int", function);
	add_symbol("a", "int", argument);
	add_symbol("b", "int", argument);

	// Memory
	add_symbol("Memory", "Memory", class);
	add_symbol("init", "void", function);

	add_symbol("peek", "int", function);
	add_symbol("address", "int", argument);

	add_symbol("poke", "void", function);
	add_symbol("address", "int", argument);
	add_symbol("value", "int", argument);

	add_symbol("alloc", "int", function);
	add_symbol("size", "int", argument);

	add_symbol("deAlloc", "void", function);
	add_symbol("o", "Array", argument);

	// Output
	add_symbol("Output", "Output", class);
	add_symbol("charMaps", "Array", static_var);
	add_symbol("init", "void", function);
	add_symbol("initMap", "void", function);

	add_symbol("create", "void", function);
	add_symbol("index", "int", argument);
	add_symbol("a", "int", argument);
	add_symbol("b", "int", argument);
	add_symbol("c", "int", argument);
	add_symbol("d", "int", argument);
	add_symbol("e", "int", argument);
	add_symbol("f", "int", argument);
	add_symbol("g", "int", argument);
	add_symbol("h", "int", argument);
	add_symbol("i", "int", argument);
	add_symbol("j", "int", argument);
	add_symbol("k", "int", argument);

	add_symbol("getMap", "Array", function);
	add_symbol("c", "char", argument);

	add_symbol("moveCursor", "void", function);
	add_symbol("i", "int", argument);
	add_symbol("j", "int", argument);

	add_symbol("printChar", "void", function);
	add_symbol("c", "char", argument);

	add_symbol("printString", "void", function);
	add_symbol("s", "String", argument);

	add_symbol("printInt", "void", function);
	add_symbol("i", "int", argument);

	add_symbol("println", "void", function);
	add_symbol("backSpace", "void", function);

	// Screen
	add_symbol("Screen", "Screen", class);
	add_symbol("init", "void", function);
	add_symbol("clearScreen", "void", function);

	add_symbol("setColor", "void", function);
	add_symbol("b", "boolean", argument);

	add_symbol("drawPixel", "void", function);
	add_symbol("x", "int", argument);
	add_symbol("y", "int", argument);

	add_symbol("drawLine", "void", function);
	add_symbol("x1", "int", argument);
	add_symbol("y1", "int", argument);
	add_symbol("x2", "int", argument);
	add_symbol("y2", "int", argument);

	add_symbol("drawRectangle", "void", function);
	add_symbol("x1", "int", argument);
	add_symbol("y1", "int", argument);
	add_symbol("x2", "int", argument);
	add_symbol("y2", "int", argument);

	add_symbol("drawCircle", "void", function);
	add_symbol("x", "int", argument);
	add_symbol("y", "int", argument);
	add_symbol("r", "int", argument);

	// String
	add_symbol("String", "String", class);

	add_symbol("new", "String", constructor);
	add_symbol("maxLength", "int", argument);
	
	add_symbol("dispose", "void", method);
	add_symbol("length", "int", method);

	add_symbol("charAt", "char", method);
	add_symbol("j", "int", argument);

	add_symbol("setCharAt", "void", method);
	add_symbol("j", "int", argument);
	add_symbol("c", "char", argument);

	add_symbol("appendChar", "String", method);
	add_symbol("c", "char", argument);

	add_symbol("eraseLastChar", "void", method);
	add_symbol("intValue", "int", method);

	add_symbol("setInt", "void", method);
	add_symbol("val", "int", argument);

	add_symbol("newLine", "char", function);
	add_symbol("backSpace", "char", function);
	add_symbol("doubleQuote", "char", function);

	// Sys
	add_symbol("Sys", "Sys", class);
	add_symbol("init", "void", function);
	add_symbol("halt", "void", function);

	add_symbol("wait", "void", function);
	add_symbol("duration", "int", argument);

	add_symbol("error", "void", function);
	add_symbol("errorCode", "int", argument);
}

void free_table() {
	for (int i = 0; i < num_classes; ++i) {
		free(symbol_table[i]);
		symbol_table[i] = NULL;
	}

	num_classes = 0;
}

void set_second_round() {
	first_round = FALSE;
}

bool is_second_round() {
	return !first_round;
}