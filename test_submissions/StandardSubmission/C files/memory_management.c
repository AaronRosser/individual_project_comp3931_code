#include "user/memory_management.h"

typedef uint8 bool;
#define TRUE 1
#define FALSE 0

#define NULL 0
#define ALIGN sizeof(uint)
#define PAGE_SIZE 4096

#define BUFFER_OFFSET sizeof(uint)

#define setAllocated(node) (node->sizeAllocated |= 0x01)
#define setUnallocated(node) (node->sizeAllocated &= 0xFFFFFFFE)
#define isAllocated(node) (node->sizeAllocated & 0x00000001)

#define setSize(node, size) node->sizeAllocated = isAllocated(node) | (0xFFFFFFFE & (size))
#define getSize(node) (node->sizeAllocated & 0xFFFFFFFE)
#define getNext(node) (node + (((getSize(node)) + sizeof(uint)) / ALIGN))

/*
 * Since the minimum size is 4 bytes
 * the least significant bit in not used
 * so the sizes least significant bit can be used
 * to specify 1 = allocated, 0 = unallocated 
 */

typedef struct NodeStruct {
    uint sizeAllocated;
    char mem[];
} Node;

static char *_heapStart = 0;
static char *_heapEnd = 0;

static void InitHeap() {
    _heapStart = sbrk(PAGE_SIZE);
    if (_heapStart == (void *) -1)
        return;
    _heapEnd = _heapStart + PAGE_SIZE;

    Node *n = (Node*) _heapStart;
    setSize(n, PAGE_SIZE - BUFFER_OFFSET);
    setUnallocated(n);
}

static void defragmentAll() {
    Node *start = (Node*) _heapStart;
    for (Node *n = start; n < (Node*) _heapEnd; n = getNext(n)) {
        if (isAllocated(n))
            continue;
            
        for (Node *afterN = getNext(n); afterN < (Node*) _heapEnd; afterN = getNext(n)) {
            if (isAllocated(afterN))
                break;
            
            uint newSize = getSize(n) + BUFFER_OFFSET + getSize(afterN);
            setSize(n, newSize);
        }
    }
}

static void tryShrinkHeap(Node *n) {
    while (n < (Node*)(_heapEnd - PAGE_SIZE)) {
        // If n isn't last node
        if (getNext(n) < (Node*)_heapEnd)
            return;

        // Free page and reduce node size
        _heapEnd = sbrk(-PAGE_SIZE) - PAGE_SIZE;
        int oldNSize = getSize(n);
        setSize(n, oldNSize - PAGE_SIZE);
    }
}

/*
 * Sets memory unallocated and defragments
 * all nodes
 */
void _free(void *ap) {
    if (ap == NULL)
        return;

    Node *n = ap;
    n -= 1;
    setUnallocated(n);

    defragmentAll();
    tryShrinkHeap(n);
}

static void splitNode(Node *n, uint nbytes) {
    // Return if split node would be too small
    int leftOverBytes = getSize(n) - nbytes - BUFFER_OFFSET;
    if (leftOverBytes <= (int) ALIGN)
        return;

    setSize(n, nbytes);

    Node *newN = getNext(n);
    setSize(newN, leftOverBytes);
}

/* 
 * Grows heap
 * Returns new last node
 */
static Node *growHeap(Node *lastN) {
    char *oldHeapEnd = sbrk(PAGE_SIZE);
    if (oldHeapEnd == (void *) -1)
        return FALSE;
    uint size = getSize(lastN);

    // If unallocated all remaining space for last
    if (!isAllocated(lastN))
        setSize(lastN, size + PAGE_SIZE);
    else
    {
        lastN = getNext(lastN);
        // Must be at the start of new page
        setSize(lastN, PAGE_SIZE - BUFFER_OFFSET);
    }

    _heapEnd = oldHeapEnd + PAGE_SIZE;
    return lastN;
}

static Node *findFreeNode(uint nbytes) {
    Node *n = (Node*) _heapStart;
    while (n < (Node*) _heapEnd) {
        // If current node has enough space
        if (getSize(n) >= nbytes && !isAllocated(n))
            return n;

        // In event not enough space n = last element
        if (getNext(n) >= (Node*) _heapEnd) {
            n = growHeap(n);
            if (n == NULL)
                return NULL;
        } else 
            n = getNext(n);
    }
    
    return n;
}

void* _malloc(uint nbytes) {
    if (nbytes == 0)
        return NULL;

    if (_heapStart == 0)
        InitHeap();

    if (_heapStart == (void *) -1)
        return NULL;

    if (nbytes < ALIGN)
        nbytes = ALIGN;

    // Ensure size aligns
    if (nbytes % ALIGN > 0)
        nbytes = (((nbytes / ALIGN) + 1) * ALIGN);

    Node *n = findFreeNode(nbytes);
    if (n == NULL)
        return NULL;
    splitNode(n, nbytes);
    setAllocated(n);

    return &(n->mem);
}