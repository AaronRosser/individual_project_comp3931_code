#include "kernel/types.h"
#include "user/user.h"
#include "kernel/fcntl.h"

#define MAX_INPUT_LEN 512

typedef int bool;
#define TRUE 1
#define FALSE 0

typedef struct CommandStruct {
	char *programName;
	char *args;
	char *fileNameIn;
	char *fileNameOut;
	bool firstCommand;
} Command;

#define COMMAND_ARRAY_CAPACITY 50
typedef struct CommandArrayStruct {
	Command **arr;
	int count;
} CommandArray;

Command *initCommand() {
	Command *command = malloc(sizeof(Command));
	command->programName = 0;
	command->args = 0;
	command->fileNameIn = 0;
	command->fileNameOut = 0;
	command->firstCommand = FALSE;
	return command;
}

void freeCommands(CommandArray *commandsArr) {
	for (int i = 0; i < commandsArr->count; i++) {
		Command *c = commandsArr->arr[i];
		if (c->programName != 0)
			free(c->programName);
		if (c->args != 0)
			free(c->args);
		if (c->fileNameIn != 0)
			free(c->fileNameIn);
		if (c->fileNameOut != 0)
			free(c->fileNameOut);
		free(c);
	}	
	free(commandsArr->arr);
	free(commandsArr);
}

//! String array
#define STR_ARRAY_CAPACITY 50
typedef struct StrArrayStruct {
	char **arr;
	int count;
	int offset;
} StrArray;

StrArray *initStrArray() {
	StrArray *strArr = malloc(sizeof(StrArray));
	strArr->arr = malloc(sizeof(char*) * STR_ARRAY_CAPACITY);
	strArr->offset = 0;
	strArr->count = 0;

	return strArr;
}

void freeArray(StrArray *stdArr) {
	for (int i = 0; i < stdArr->count; i++)
		free(stdArr->arr[i]);
	free(stdArr->arr);
	free(stdArr);
}

char *mallocStrcpy(const char *str) {
	char *newStr = malloc(sizeof(char) * (strlen(str) + 1));
	strcpy(newStr, str);
	return newStr;
}

void strCombine(char *buffer, const char *str) {
	int len = strlen(buffer);
	
	if (len == 0) {
		strcpy(buffer, str);
	} else {
		buffer[len] = ' ';
		strcpy(buffer + len + 1, str);
	}
}

bool isOperator(const char *chr) {
	return chr[0] == '<' || chr[0] == '>' || chr[0] == '|'|| chr[0] == ';';
}

void error(char *errMsg) {
	fprintf(2, "An error occurred: %s\n", errMsg);
}

void cd(char *args) {
	if (chdir(args) < 0) {
		error("cd failed, does the directory exist?");
	}
}

void run(CommandArray *commands) {
	int inPipe[2] = { -1, -1 };
	int outPipe[2] = { -1, -1 };

	for (int i = 0; i < commands->count; i++) {
		Command *c = commands->arr[i];
		// Handle cd
		if (strcmp(c->programName, "cd") == 0) {
			cd(c->args);
			continue;
		}

		// Handle exit
		if (strcmp(c->programName, "exit") == 0) {
			exit(0);
		}

		char **args = malloc(sizeof(char*) * 3);
		args[0] = c->programName;
		args[1] = c->args;
		args[2] = 0;

		int remainingCommands = commands->count - i - 1;
		if (remainingCommands > 0 && !commands->arr[i + 1]->firstCommand) {
			// Open right pipe will be moved to left
			if (pipe(outPipe) < 0)
				error("Pipe failed to open");
		}

		int pid = fork();
		if (pid == 0) {
			// If command before opened pipe
			if (inPipe[0] != -1) {
				close(0);
				dup(inPipe[0]);
				
				close(inPipe[0]);
				close(inPipe[1]);
			}

			// If a command follows
			if (outPipe[0] != -1) {
				close(1);
				dup(outPipe[1]);

				close(outPipe[0]);
				close(outPipe[1]);
			}

			if (c->fileNameIn != 0) {
				close(0);
				if (open(c->fileNameIn, O_RDONLY) < 0) {
					fprintf(2, "Failed to open file %s\n", c->fileNameIn);
					exit(1);
				}
			}

			if (c->fileNameOut != 0) {
				close(1);
				if(open(c->fileNameOut, O_CREATE | O_WRONLY) < 0) {
					fprintf(2, "Failed to open file %s\n", c->fileNameOut);
					exit(1);
				}
			}

			exec(c->programName, args);

			// If executed error occurred
			fprintf(2, "Exec of %s failed\n", c->programName);
			exit(1);
		} else if (pid < 0) {
			error("Fork failed");
		}

		// Close pipes
		if (inPipe[0] != -1) {
			close(inPipe[0]);
			close(inPipe[1]);
		}
		// Move right pipe to left
		inPipe[0] = outPipe[0];
		inPipe[1] = outPipe[1];

		outPipe[0] = -1;
		outPipe[1] = -1;

		free(args);
	}

	for (int i = 0; i < commands->count; i++) {
		wait(0);
	}
}

CommandArray *extractCommands(const StrArray *strArr) {
	CommandArray *cArr = malloc(sizeof(CommandArray));
	cArr->arr = malloc(sizeof(Command*) * COMMAND_ARRAY_CAPACITY);
	cArr->count = 0;

	for (int i = 0; i < strArr->count; i++) {
		/*
		 * Each cycle starts with program, ends with program, args, file name or pipe
		 * When executing every command after 1st has incoming pipe
		 * And every command but last has outgoing pipe
		 */
		Command *current = cArr->arr[cArr->count] = initCommand();
		current->programName = mallocStrcpy(strArr->arr[i]);
		if (i == 0 || strArr->arr[i - 1][0] == ';')
			current->firstCommand = TRUE;

		int remaining = strArr->count - i - 1;
		if (remaining == 0) {
			cArr->count++;
			continue;
		}

		int offset = 1;
		if (!isOperator(strArr->arr[i + 1])) {
			current->args = mallocStrcpy(strArr->arr[i + 1]);
			offset++;
		}

		while (i + offset < strArr->count) {
			char operator = strArr->arr[i + offset][0];
			char *fileProgramName = strArr->arr[i + offset + 1];

			if (operator == '|' || operator == ';')
				break;

			if (operator == '<')
				current->fileNameIn = mallocStrcpy(fileProgramName);
			else if (operator == '>')
				current->fileNameOut = mallocStrcpy(fileProgramName);
			
			offset += 2;
		}

		i += offset;
		cArr->count++;
	}

	return cArr;
}

StrArray *joinArgs(StrArray *strArr) {
	StrArray *newArr = initStrArray();

	char buffer[MAX_INPUT_LEN];

	for (int i = 0; i < strArr->count; i++) {
		char *cur = strArr->arr[i];
		// Direct copy operators and programs filenames
		if (isOperator(cur) || isOperator(strArr->arr[i - 1]) || i == 0) {
			newArr->arr[newArr->count] = mallocStrcpy(cur);
			newArr->count++;
			continue;
		}

		// Append to buffer
		strCombine(buffer, cur);

		if (i + 1 >= strArr->count || isOperator(strArr->arr[i + 1])) {
			newArr->arr[newArr->count] = mallocStrcpy(buffer);
			newArr->count++;

			buffer[0] = '\0';
		}
	}

	freeArray(strArr);
	return newArr;
}

/*
 * Replaces each space with string terminator
 * Returns number of strings in buffer
 */
int replaceSpaces(char *inputStr)
{
	int inputLen = strlen(inputStr);

	// If start with newine or space
	if (inputLen == 1 || inputStr[0] == ' ')
		return 0;
	
	// Replace new line with space
	inputStr[inputLen - 1] = ' ';

	// Split buffer
	int count = 0;
	bool speechMarks = FALSE;
	
	for (int i = 1; i < inputLen; i++)
	{
		// Return at end of string
		if (inputStr[i] == '\0')
			return count;

		// Return on double space
		if (inputStr[i - 1] == '\0' && inputStr[i] == ' ')
			return count;

		// Toggle speech marks flag
		if (inputStr[i] == '"') {
			speechMarks = speechMarks == FALSE;
			continue;
		}

		// If space and not within speechmarks
		if (inputStr[i] == ' ' && speechMarks == FALSE) {
			inputStr[i] = '\0';
			count++;
		}
	}

	return count;
}

// Copies terminated strings to new array
StrArray *splitInputBuffer(char *inputStr, int numberOfWords) {
	StrArray *strArr = initStrArray();

	int offset = 0;
	for (int i = 0; i < numberOfWords; i++) {
		int len = strlen(inputStr + offset);
		bool endsWithSpeechMark = FALSE;

		// Skip starting speech mark
		if (inputStr[offset] == '"') {
			offset += 1;
			len -= 1;
		}

		// Skip ending speech mark
		if (inputStr[offset + len - 1] == '"') {
			inputStr[offset + len - 1] = '\0';
			len -= 1;
			endsWithSpeechMark = TRUE;
		}

		strArr->arr[i] = malloc(sizeof(char) * len);
		strcpy(strArr->arr[i], inputStr + offset);

		// Add 1 for terminator
		offset += len + endsWithSpeechMark + 1;
		strArr->count++;
	}

	return strArr;
}

StrArray *prompt()
{
	printf(">>> ");

	char buffer[MAX_INPUT_LEN];
	gets(buffer, MAX_INPUT_LEN);

	int numWords = replaceSpaces(buffer);

	StrArray *temp = splitInputBuffer(buffer, numWords);
	return joinArgs(temp);
}

//! Main
int main(int argc, char *argv[])
{
	while (1)
	{
		StrArray *strArr = prompt();

		CommandArray *commands = extractCommands(strArr);
		run(commands);

		freeArray(strArr);
		freeCommands(commands);
	}
}