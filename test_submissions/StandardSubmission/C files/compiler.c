/************************************************************************
University of Leeds
School of Computing
COMP2932- Compiler Design and Construction
Compiler Module

I confirm that the following code has been developed and written by me and it is entirely the result of my own work.
I also confirm that I have not copied any parts of this program from another person or any other source or facilitated someone to copy this program from me.
I confirm that I will not publish the program online or share it with anyone without permission of the module leader.

Student Name:			Aaron Rosser
Student ID:				201319759
Email:					sc19ar@leeds.ac.uk
Date Work Commenced:	27/04/21
*************************************************************************/
// Fix error: ‘DT_REG’ undeclared
#define _DEFAULT_SOURCE

#include "compiler.h"
#include "symbols.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h> 

// Array for compiled code
char *compiled_code[1000];
int num_compiled_commands = 0;

void write_command(const char *command) {
	char *command_cpy = malloc(sizeof(char[strlen(command) + 1]));
	strcpy(command_cpy, command);
	compiled_code[num_compiled_commands++] = command_cpy;
}

void free_commands() {
	for (int i = 0; i < num_compiled_commands; ++i) {
		free(compiled_code[i]);
	}
	num_compiled_commands = 0;
}

void print_commands() {
	printf("Compiled code:\n");
	for (int i = 0; i < num_compiled_commands; ++i) {
		printf("%s\n", compiled_code[i]);
	}
}

int InitCompiler ()
{
	init_table();
	return 1;
}

ParserInfo compile_file(char *f_name) {
	if (InitParser(f_name) == 0)
		panic("init parser failed");

	ParserInfo p = Parse();
	StopParser();
	return p;
}

void save_commands_to_file(const char *jack_f_name) {
	char vm_f_name[512];
	strcpy(vm_f_name, jack_f_name);
	if (strlen(vm_f_name) < 5)
		panic("Invalid jack file path");

	strcpy(vm_f_name + strlen(vm_f_name) - 5, ".vm");

	FILE *f = fopen(vm_f_name, "wb");

	for (int i = 0; i < num_compiled_commands; ++i) {
		fputs(compiled_code[i], f);
		fputs("\n", f);
	}
	
	fclose(f);
}

ParserInfo compile(char* dir_name)
{
	ParserInfo pi;
	DIR *d;
	struct dirent *dir;
	char file_path[512];

	// Parse and add symbols
	if (!(d = opendir(dir_name)))
		panic("Failed to open directory");
	
	while ((dir = readdir(d)) != NULL) {
		int f_name_len = strlen(dir->d_name) - 5;

		if (dir->d_type == DT_REG && f_name_len >= 0 && strcmp(dir->d_name + f_name_len, ".jack") == 0) {
			sprintf(file_path, "%s/%s", dir_name, dir->d_name);
			
			pi = compile_file(file_path);
			free_commands();
			if (pi.er != none)
				return pi;
		}
	}
	closedir(d);

	
	set_second_round();

	// Parse and generate code
	if (!(d = opendir(dir_name)))
		panic("Failed to open directory");
	
	while ((dir = readdir(d)) != NULL) {
		int f_name_len = strlen(dir->d_name) - 5;

		if (dir->d_type == DT_REG && f_name_len >= 0 && strcmp(dir->d_name + f_name_len, ".jack") == 0) {
			sprintf(file_path, "%s/%s", dir_name, dir->d_name);
			
			pi = compile_file(file_path);
			if (pi.er != none) {
				free_commands();
				return pi;
			}

			save_commands_to_file(file_path);
			free_commands();
		}
	}
	closedir(d);
	
	return pi;
}

int StopCompiler ()
{
	free_table();
	free_commands();
	return 1;
}

void PrintError (ParserInfo pn)
{
	if (pn.er == none)
		printf ("No errors\n");
	else
		printf ("Error in file %s line %i at or near %s: %s\n" , pn.tk.fl , pn.tk.ln , pn.tk.lx , ErrorString(pn.er));
}

#ifndef TEST_COMPILER
int main ()
{
	InitCompiler ();
	ParserInfo p = compile ("Average");
	PrintError (p);

	print_commands();
	StopCompiler ();
	return 1;
}
#endif
