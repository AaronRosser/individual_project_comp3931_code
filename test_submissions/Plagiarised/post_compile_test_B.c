// Diagraph #
%:include <stdio.h>

// Long int = long
typedef long int LONG_INT;

int bin_search(LONG_INT* arr, int size, LONG_INT x) <%
    // Combinied declaration / renamed identifiers
    int l = 0, m = 0, r = size - 1;

    while (l <= r) <%
        m = (l + r) / 2;

        // One true brace style with diagraph braces
        if (arr<:m:> < x) <%
            l = m + 1;
        %> else if (arr<:m:> > x) <%
            r = m - 1;
        %> else <%
            return m;
        %>
    %>

    return -1;
%>

int main(int arg_count, char *args_arr<::>)
{
    // Identical constants / explicit array size
    LONG_INT nums<:16:> = <%
        0, 10, 20, 30, 0x28, 0x32, 0x3C, 0106, 
        0120, 0132, 'd', 'n', 'x', 130, 140l, 150L
    %>;

    int res = bin_search(nums, 16, 30);
    printf("30 in index: %d\n", res);

    // ++i is same as i += 1
    for (int i = 0; i < 16; i += 1) <%
        printf("%ld\n", nums<:i:>);
    %>

    // Identical constants - binary exponent and 
    // scientific notation
    printf("168.5 * 1.5 = %lf\n", 0xA.88p4 * 15e-1);
}



