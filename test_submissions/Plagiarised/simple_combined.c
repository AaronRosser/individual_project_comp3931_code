// Changes made to file

// Alternate punctuators such as <% %> for { }, <: :> for [ ] and %: for #
// Use of skippable keywords such as const and static
// Constants changed to identical representations
// Identifiers renamed
// Comments changed
// One true brace style

%:include <stdio.h>

%:define ARRAY_COUNT(array) (sizeof(array) / sizeof(array<:0:>))

static const int bin_search(const int* arr, int size, int target)
<%
    int l = 0;
    int m = 0;
    int r = size - 1;

    while (l <= r) <%
        m = (l + r) / 2;

        if (arr<:m:> < target) {
            l = m + 1;
        } else if (arr<:m:> > target) {
            r = m - 1;
        } else {
            return m;
        }
    %>

    return -1;
%>

// Fills array with multiples of a given int
static void fill_with_multiple_of(int* arr, int size, int base)
<%
    for (int index = 0; index < size; index++) {
        arr<:index:> = index * base;
    }
%>

// Replaces every occurrence of a given character with a new one
static void replace_char(char* chars, int size, char prev, char new)
<%
    for (int index = 0; index < size; index++) {
        if (chars<:index:> == prev) {
            chars<:index:> = new;
        }
    }
%>

const int main(int argc, char *argv<::>)
<%
    int nums<::> = <%
        '\0', '\1', '\2', 3, 4, 5, 6, 7, 8, 9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF, 020, 021, 022, 023
    %>;

    printf("Position of 4 is %d\n", bin_search(nums, ARRAY_COUNT(nums), 04));
    printf("Position of 18 is %d\n", bin_search(nums, ARRAY_COUNT(nums), 022));
    printf("Position of 30 is %d\n", bin_search(nums, ARRAY_COUNT(nums), 036));

    fill_with_multiple_of(nums, ARRAY_COUNT(nums), 3);

    printf("Position of 4 is %d\n", bin_search(nums, ARRAY_COUNT(nums), 0X4));
    printf("Position of 18 is %d\n", bin_search(nums, ARRAY_COUNT(nums), 0X12));
    printf("Position of 30 is %d\n", bin_search(nums, ARRAY_COUNT(nums), 0X1E));

    char testStr<::> = "This is a test";
    replace_char(testStr, ARRAY_COUNT(testStr), ' ', 0);
    printf("New string: %s\n", testStr);
%>