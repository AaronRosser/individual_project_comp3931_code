// Changes made to file

// All ints were swapped for the identical typedef INT_32
// ARRAY_COUNT for swapped for a constant defined array size

#include <stdio.h>

#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))

// Replace all INT_32 with int
typedef int INT_32;

#define NUMBERS_SIZE 20

INT_32 binarySearch(INT_32* searchArr, INT_32 arrSize, INT_32 x)
{
    INT_32 left = 0;
    INT_32 mid = 0;
    INT_32 right = arrSize - 1;

    while (left <= right) {
        mid = (left + right) / 2;

        if (searchArr[mid] < x)
            left = mid + 1;
        else if (searchArr[mid] > x)
            right = mid - 1;
        else
            return mid;
    }

    return -1;
}

void fillWithMultipleOf(INT_32* intArr, INT_32 arrSize, INT_32 x)
{
    for (int i = 0; i < arrSize; i++)
        intArr[i] = i * x;
}

void replaceChar(char* characters, INT_32 arrSize, char old, char new)
{
    for (INT_32 i = 0; i < arrSize; i++)
        if (characters[i] == old)
            characters[i] = new;
}

INT_32 main(INT_32 argc, char *argv[])
{
    static INT_32 numbers[] = {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
    };

    printf("Position of 4 is %d\n", binarySearch(numbers, NUMBERS_SIZE, 4));
    printf("Position of 18 is %d\n", binarySearch(numbers, NUMBERS_SIZE, 18));
    printf("Position of 30 is %d\n", binarySearch(numbers, NUMBERS_SIZE, 30));

    // Fills numbers array with first 20 multiples of 3
    fillWithMultipleOf(numbers, NUMBERS_SIZE, 3);

    printf("Position of 4 is %d\n", binarySearch(numbers, NUMBERS_SIZE, 4));
    printf("Position of 18 is %d\n", binarySearch(numbers, NUMBERS_SIZE, 18));
    printf("Position of 30 is %d\n", binarySearch(numbers, NUMBERS_SIZE, 30));

    char testString[] = "This is a test";
    // Terminate the string after the first space
    replaceChar(testString, ARRAY_COUNT(testString), ' ', '\0');
    printf("New string: %s\n", testString);
}