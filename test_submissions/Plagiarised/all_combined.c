// Changes made to file

// Alternate punctuators
// Use of skippable keywords such as const and static
// Constants changed to identical representations
// Identifiers renamed
// Typedef of int
// Comments changed
// Explicit array declaration
// Removal of sizeof operators
// Moving variable declarations to the start of scopes
// One true brace style
// Switch i++ for ++i and i += 1

%:include <stdio.h>

typedef int INT_32;

// Comments

static const INT_32 bin_search(const INT_32* arr, INT_32 size, INT_32 x)
<%
    INT_32 l = 0, m = 0, r = size - 1;

    while (l <= r) <%
        m = (l + r) / 2;

        if (arr<:m:> < x) <%
            l = m + 1;
        %> else if (arr<:m:> > x) <%
            r = m - 1;
        %> else <%
            return m;
        %>
    %>

    return -1;
%>

static void fill_with_multiple_of(INT_32* arr, INT_32 size, INT_32 x)
<%
    INT_32 i;
    for (i = 0; i < size; i += 1) <%
        arr<:i:> = i * x;
    %>
%>

static void replace_char(char* chars, INT_32 size, char old, char new)
<%
    INT_32 i;
    for (i = 0; i < size; ++i) <%
        if (chars<:i:> == old) <%
            chars<:i:> = new;
        %>
    %>
%>

INT_32 main(INT_32 arg_count, char *arg_arr<::>)
<%
    INT_32 nums<:20:> = <%
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF, 020, 021, 022, 023
    %>;

    printf("Position of 4 is %d\n", bin_search(nums, 20, 4));
    printf("Position of 18 is %d\n", bin_search(nums, 20, 18));
    printf("Position of 30 is %d\n", bin_search(nums, 20, 30));

    fill_with_multiple_of(nums, 20, 3);

    printf("Position of 4 is %d\n", bin_search(nums, 20, 4));
    printf("Position of 18 is %d\n", bin_search(nums, 20, 18));
    printf("Position of 30 is %d\n", bin_search(nums, 20, 30));

    char str<:15:> = "This is a test";
    replace_char(str, 15, ' ', 0);
    printf("New string: %s\n", str);
%>