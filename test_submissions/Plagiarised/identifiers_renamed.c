// Changes made to file

// Identifiers renamed

#include <stdio.h>

#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))

// K&R brace style

int binary_search(const int* arr, int size, int target)
{
    int l = 0;
    int m = 0;
    int r = size - 1;

    while (l <= r) {
        m = (l + r) / 2;

        if (arr[m] < target)
            l = m + 1;
        else if (arr[m] > target)
            r = m - 1;
        else
            return m;
    }

    return -1;
}

void fill_with_multiple_of(int* arr, int size, int base)
{
    for (int i = 0; i < size; i++)
        arr[i] = i * base;
}

void replace_char(char* chars, int size, char old, char new)
{
    for (int i = 0; i < size; i++)
        if (chars[i] == old)
            chars[i] = new;
}

int main(int argc, char *argv[])
{
    int nums[] = {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
    };

    printf("Position of 4 is %d\n", binary_search(nums, ARRAY_COUNT(nums), 4));
    printf("Position of 18 is %d\n", binary_search(nums, ARRAY_COUNT(nums), 18));
    printf("Position of 30 is %d\n", binary_search(nums, ARRAY_COUNT(nums), 30));

    // Fills numbers array with first 20 multiples of 3
    fill_with_multiple_of(nums, ARRAY_COUNT(nums), 3);

    printf("Position of 4 is %d\n", binary_search(nums, ARRAY_COUNT(nums), 4));
    printf("Position of 18 is %d\n", binary_search(nums, ARRAY_COUNT(nums), 18));
    printf("Position of 30 is %d\n", binary_search(nums, ARRAY_COUNT(nums), 30));

    char replace_test[] = "This is a test";
    // Terminate the string after the first space
    replace_char(replace_test, ARRAY_COUNT(replace_test), ' ', '\0');
    printf("New string: %s\n", replace_test);
}