#include <stdio.h>

#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))

int binarySearch(long* searchArr, int arrSize, long x)
{
    int left = 0;
    int mid = 0;
    int right = arrSize - 1;

    while (left <= right) {
        mid = (left + right) / 2;

        if (searchArr[mid] < x)
            left = mid + 1;
        else if (searchArr[mid] > x)
            right = mid - 1;
        else
            return mid;
    }

    return -1;
}

int main(int argc, char *argv[])
{
    long numbers[] = {
        0, 10, 20, 30, 40, 50, 60, 70, 
        80, 90, 100, 110, 120, 130, 140, 150
    };

    int result = binarySearch(numbers, ARRAY_COUNT(numbers), 30);
    printf("30 in index: %d\n", result);

    for (int i = 0; i < ARRAY_COUNT(numbers); ++i)
        printf("%ld\n", numbers[i]);

    printf("168.5 * 1.5 = %lf\n", 168.5 * 1.5);
}













