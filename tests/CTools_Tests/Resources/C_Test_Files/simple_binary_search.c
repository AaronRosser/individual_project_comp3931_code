#include <stdio.h>

#define ARRAY_COUNT(array) (sizeof(array) / sizeof(array[0]))

// K&R brace style

int binarySearch(int* searchArr, int arrSize, int x)
{
    int left = 0;
    int mid = 0;
    int right = arrSize - 1;

    while (left <= right) {
        mid = (left + right) / 2;

        if (searchArr[mid] < x)
            left = mid + 1;
        else if (searchArr[mid] > x)
            right = mid - 1;
        else
            return mid;
    }

    return -1;
}

void fillWithMultipleOf(int* intArr, int arrSize, int x)
{
    for (int i = 0; i < arrSize; i++)
        intArr[i] = i * x;
}

void replaceChar(char* characters, int arrSize, char old, char new)
{
    for (int i = 0; i < arrSize; i++)
        if (characters[i] == old)
            characters[i] = new;
}

int main(int argc, char *argv[])
{
    int numbers[] = {
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19
    };

    printf("Position of 4 is %d\n", binarySearch(numbers, ARRAY_COUNT(numbers), 4));
    printf("Position of 18 is %d\n", binarySearch(numbers, ARRAY_COUNT(numbers), 18));
    printf("Position of 30 is %d\n", binarySearch(numbers, ARRAY_COUNT(numbers), 30));

    // Fills numbers array with first 20 multiples of 3
    fillWithMultipleOf(numbers, ARRAY_COUNT(numbers), 3);

    printf("Position of 4 is %d\n", binarySearch(numbers, ARRAY_COUNT(numbers), 4));
    printf("Position of 18 is %d\n", binarySearch(numbers, ARRAY_COUNT(numbers), 18));
    printf("Position of 30 is %d\n", binarySearch(numbers, ARRAY_COUNT(numbers), 30));

    char testString[] = "This is a test";
    // Terminate the string after the first space
    replaceChar(testString, ARRAY_COUNT(testString), ' ', '\0');
    printf("New string: %s\n", testString);
}