using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using CTools;
using NUnit.Framework;
using CTools.Lexer;

namespace CTools_Tests;

public class LexerTests
{
    private const string CTestFolder = "Resources/C_Test_Files/";
    private const string JsonTokenFolder = "Resources/JSON_Token_Files/";
    private static readonly JsonSerializerOptions JsonOptions = new JsonSerializerOptions {
        Converters ={
            new JsonStringEnumConverter()
        },
        WriteIndented = true,
        NumberHandling = JsonNumberHandling.AllowNamedFloatingPointLiterals
    };
    
    [SetUp]
    public void Setup()
    {
        
    }

    private void GenerateSaveTokens(string fileName)
    {
        string cFilePath = $"{CTestFolder}{fileName}.c";
        string jsonFilePath = $"{JsonTokenFolder}{fileName}.json";
        
        Lexer lexer = new Lexer(cFilePath);
        var generatedTokens = lexer.GetAllTokens();
        
        var tokensJson = JsonSerializer.Serialize(generatedTokens, JsonOptions);
        File.WriteAllText(jsonFilePath, tokensJson);
    }

    private List<Token> CompareTokens(string fileName)
    {
        string cFilePath = $"{CTestFolder}{fileName}.c";
        string jsonFilePath = $"{JsonTokenFolder}{fileName}.json";
        
        var correctTokens = JsonSerializer.Deserialize<List<Token>>(File.ReadAllText(jsonFilePath), JsonOptions);
        Assert.NotNull(correctTokens, $"Missing tokens JSON file for {fileName}.c");

        Lexer lexer = new Lexer(cFilePath);
        var generatedTokens = lexer.GetAllTokens();
        
        if (correctTokens!.Count != generatedTokens.Count)
            Assert.Fail($"Incorrect number of generated tokens in {fileName}.c");
        
        foreach (var (correctToken, generatedToken) in correctTokens.Zip(generatedTokens))
        {
            if (correctToken != generatedToken)
                Assert.Fail($"Expected token {correctToken} but got {generatedToken}");
        }

        return generatedTokens;
    }

    private void AssertLexerException(string fileName)
    {
        string cFilePath = $"{CTestFolder}{fileName}.c";
        Lexer lexer = new Lexer(cFilePath);

        try
        {
            var generatedTokens = lexer.GetAllTokens();
            Assert.Fail("Expected LexerException but was successful");
        }
        catch (LexerException)
        {
            
        }
    }

    [Test]
    public void TestBasicFunction()
    {
        CompareTokens("basic_function");
    }

    [Test]
    public void GenerateJsons()
    {
        // GenerateSaveTokens("basic_function");
        // GenerateSaveTokens("Lexer/Successful/character_constants");
        // GenerateSaveTokens("Lexer/Successful/comments");
        // GenerateSaveTokens("Lexer/Successful/float_constants");
        // GenerateSaveTokens("Lexer/Successful/identifiers");
        // GenerateSaveTokens("Lexer/Successful/integer_constants");
        // GenerateSaveTokens("Lexer/Successful/keywords");
        // GenerateSaveTokens("Lexer/Successful/punctuators");
        // GenerateSaveTokens("Lexer/Successful/strings");
        // GenerateSaveTokens("simple_binary_search");
        // GenerateSaveTokens("const");
        // GenerateSaveTokens("constants_changed");
    }
    
    [Test]
    public void TestCharacterConstants()
    {
        CompareTokens("Lexer/Successful/character_constants");
    }
    
    [Test]
    public void TestComments()
    {
        CompareTokens("Lexer/Successful/comments");
    }

    [Test]
    public void TestFloatConstants()
    {
        CompareTokens("Lexer/Successful/float_constants");
    }

    [Test]
    public void TestIdentifiers()
    {
        var tokens = CompareTokens("Lexer/Successful/identifiers");
        
    }
    
    [Test]
    public void TestIntegerConstants()
    {
        CompareTokens("Lexer/Successful/integer_constants");
    }
    
    [Test]
    public void TestKeywords()
    {
        CompareTokens("Lexer/Successful/keywords");
    }
    
    [Test]
    public void TestPunctuators()
    {
        CompareTokens("Lexer/Successful/punctuators");
    }

    [Test]
    public void TestStrings()
    {
        CompareTokens("Lexer/Successful/strings");
    }
    
    [Test]
    public void TestExceptionEmptyCharacterConstant()
    {
        AssertLexerException("Lexer/Exception/empty_character_constant");
    }
    
    [Test]
    public void TestExceptionEofInCharConstant()
    {
        AssertLexerException("Lexer/Exception/eof_in_char_constant");
    }
    
    [Test]
    public void TestExceptionEofInHeaderName()
    {
        AssertLexerException("Lexer/Exception/eof_in_header_name");
    }
    
    [Test]
    public void TestExceptionEofInString()
    {
        AssertLexerException("Lexer/Exception/eof_in_string");
    }
    
    [Test]
    public void TestExceptionInvalidHexEscape()
    {
        AssertLexerException("Lexer/Exception/invalid_hex_escape");
    }

    [Test]
    public void TestExceptionInvalidUniversalCharacterName()
    {
        AssertLexerException("Lexer/Exception/invalid_universal_character_name");
    }
    
    [Test]
    public void TestExceptionNewLineInCharConstant()
    {
        AssertLexerException("Lexer/Exception/new_line_in_char_constant");
    }
    
    [Test]
    public void TestExceptionNewLineInHeaderName()
    {
        AssertLexerException("Lexer/Exception/new_line_in_header_name");
    }
    
    [Test]
    public void TestExceptionNewLineInString()
    {
        AssertLexerException("Lexer/Exception/new_line_in_string");
    }
}