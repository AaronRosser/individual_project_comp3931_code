using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using CTools;
using CTools.Comparison;
using NUnit.Framework;

namespace CTools_Tests;

public class TokenComparerTests
{
    private const string JsonTokenFolder = "Resources/JSON_Token_Files/";
    private static readonly JsonSerializerOptions JsonOptions = new JsonSerializerOptions {
        Converters ={
            new JsonStringEnumConverter()
        },
        WriteIndented = true,
        NumberHandling = JsonNumberHandling.AllowNamedFloatingPointLiterals
    };
    
    [SetUp]
    public void Setup()
    {
        
    }
    
    private List<Token> LoadTokens(string fileName)
    {
        string jsonFilePath = $"{JsonTokenFolder}{fileName}.json";
        
        var tokens = JsonSerializer.Deserialize<List<Token>>(File.ReadAllText(jsonFilePath), JsonOptions);
        Assert.NotNull(tokens, $"Missing tokens JSON file for {fileName}");

        return tokens!;
    }

    [Test]
    public void TestIdentical()
    {
        var tokens = LoadTokens("basic_function");
        var results = TokenComparer.CompareFileTokens(tokens, tokens);
        
        Assert.AreEqual(1, results.Count);
        Assert.AreEqual(30, results[0].NumberMatchingTokens);
        Assert.AreEqual(0,results[0].FileATokenIndexStart);
        Assert.AreEqual(0,results[0].FileBTokenIndexStart);
    }
    
    [Test]
    public void TestBinSearchConst()
    {
        var binarySearchTokens = LoadTokens("simple_binary_search");
        var constTokens = LoadTokens("const");
        var results = TokenComparer.CompareFileTokens(binarySearchTokens, constTokens);
        
        Assert.AreEqual(3, results.Count);
        Assert.AreEqual(405, results[0].NumberMatchingTokens);
        Assert.AreEqual(0,results[0].FileATokenIndexStart);
        Assert.AreEqual(0,results[0].FileBTokenIndexStart);
    }
    
    [Test]
    public void TestBinSearchConstantsChanged()
    {
        var binarySearchTokens = LoadTokens("simple_binary_search");
        var constantsChangedTokens = LoadTokens("constants_changed");
        var results = TokenComparer.CompareFileTokens(binarySearchTokens, constantsChangedTokens);
        
        Assert.AreEqual(3, results.Count);
        Assert.AreEqual(401, results[0].NumberMatchingTokens);
        Assert.AreEqual(0,results[0].FileATokenIndexStart);
        Assert.AreEqual(0,results[0].FileBTokenIndexStart);
    }
}