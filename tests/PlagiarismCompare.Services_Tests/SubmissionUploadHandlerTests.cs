using System.IO;
using System.Threading.Tasks;
using NUnit.Framework;
using PlagiarismCompare.Data.Models;

namespace PlagiarismCompare.Services_Tests;

public class SubmissionUploadHandlerTests : BaseTests
{
    [SetUp]
    public void Setup()
    {
        
    }

    public Task<Submission> TestParseSubmission(string submissionName, string fileNamePrefix, string zipFilePath, SemesterStudent? semesterStudent)
    {
        Assert.NotNull(semesterStudent);
        
        Stream fileStream = File.OpenRead(zipFilePath);
        return UploadHandler.ParseSubmission(submissionName, fileNamePrefix, fileStream, semesterStudent!);
    }

    [Test]
    public async Task TestZipUpload()
    {
        string submissionName = "simple_upload_test";
        string fileNamePrefix = "simple_upload_test";
        string zipFilePath = "Resources/simple_upload_test.zip";
        
        var semesterStudent = await Context.SemesterStudents.FindAsync(1);

        Submission submission = await TestParseSubmission(submissionName, fileNamePrefix, zipFilePath, semesterStudent);
        Assert.AreEqual(submission.CFiles.Count, 3);
        Assert.AreEqual(submission.Name, submissionName);

        string submissionsDirectory = Path.Join(BaseDirectory, "uploads/submissions/");
        Assert.That(Path.Join(submissionsDirectory, $"{submission.FileNamePrefix}.zip"), Does.Exist);
        Assert.That(Path.Join(submissionsDirectory, submission.FileNamePrefix, "ExampleSubmission", "CodeGrader.c"), Does.Exist);
        Assert.That(Path.Join(submissionsDirectory, submission.FileNamePrefix, "ExampleSubmission", "compiler.c"), Does.Exist);
        Assert.That(Path.Join(submissionsDirectory, submission.FileNamePrefix, "ExampleSubmission", "lexer.c"), Does.Exist);
    }
}