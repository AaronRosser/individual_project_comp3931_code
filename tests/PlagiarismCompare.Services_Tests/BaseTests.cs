using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;
using PlagiarismCompare.Data;
using PlagiarismCompare.Services;
using PlagiarismCompare.Services.IO;

namespace PlagiarismCompare.Services_Tests;

public class BaseTests
{
    protected readonly string BaseDirectory = Path.GetFullPath("./Resources/");
    
    protected readonly DataContext Context;
    
    protected readonly SubmissionUploadHandler UploadHandler;
    protected readonly SubmissionComparisonHandler ComparisonHandler;

    protected BaseTests()
    {
        var mockEnvironment = new Mock<IWebHostEnvironment>();
        mockEnvironment
            .Setup(m => m.WebRootPath)
            .Returns(BaseDirectory);

        var services = new ServiceCollection();
        services.AddSingleton(mockEnvironment.Object);
        services.AddDbContext<DataContext>(options => options.UseInMemoryDatabase("ServicesTestDatabase"));
        services.AddTransient<SubmissionUploadHandler>();
        services.AddTransient<SubmissionComparisonHandler>();
        var serviceProvider = services.BuildServiceProvider();
        
        Context = serviceProvider.GetService<DataContext>() ?? throw new InvalidOperationException();
        UploadHandler = serviceProvider.GetService<SubmissionUploadHandler>() ?? throw new InvalidOperationException();
        ComparisonHandler = serviceProvider.GetService<SubmissionComparisonHandler>() ?? throw new InvalidOperationException();

        DbInitializer.Initialize(Context);
    }

    public void Dispose()
    {
        Context.Dispose();
    }
}